﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using static VirtualKeyboard.CudaPSO;

namespace VirtualKeyboard
{
    struct AnimationLine
    {
        public int baseIndex;
        public int targetIndex;
        public float length;

        public AnimationLine(int baseIndex, int targetIndex, float length)
        {
            this.baseIndex = baseIndex;
            this.targetIndex = targetIndex;
            this.length = length;
        }
    }

    class Animation
    {
               
        // The main object model group.
        private static Model3DGroup mainModel3DGroup = new Model3DGroup();

        private static List<GeometryModel3D> geometryModels = new List<GeometryModel3D>();
        private static List<GeometryModel3D> spheres = new List<GeometryModel3D>();
        private static List<GeometryModel3D> cylinders = new List<GeometryModel3D>();

        // The camera.
        private static PerspectiveCamera camera;

        // The camera's current location.
        private static double CameraPhi = Math.PI / 6.0;       // 30 degrees
        private static double CameraTheta = Math.PI / 6.0;     // 30 degrees
        private static double CameraR = 7.0;

        private static double cameraX = 0.06; // 0.075
        private static double cameraY = 0.15; // 0.17
        private static double cameraZ = 1.7; // 1.75

        private static Dictionary<char, Rectangle> keyPairings = new Dictionary<char, Rectangle>();

        // The change in CameraPhi when you press the up and down arrows.
        private const double CameraDPhi = 0.1;

        // The change in CameraTheta when you press the left and right arrows.
        private const double CameraDTheta = 0.1;

        // The change in CameraR when you press + or -.
        private const double CameraDR = 0.1;

        private static Viewport3D viewport3D;

        private const int numOfLines = 20;

        private static AnimationLine[] animationLines = new AnimationLine[numOfLines * 2];

        public static void setup(Viewport3D viewport)
        {
            initializeAnimationLines();

            camera = new PerspectiveCamera();
            camera.FieldOfView = 45;
            viewport.Camera = camera;

            positionCamera();

            defineLights();

            // Create the model.
            defineModel(mainModel3DGroup);

            ModelVisual3D modelVisual = new ModelVisual3D();
            modelVisual.Content = mainModel3DGroup;

            // Display the main visual to the viewport.
            viewport.Children.Add(modelVisual);

            viewport3D = viewport;

            initializeKeyPairings();


        }

        private static void initializeAnimationLines()
        {

            animationLines[0] = new AnimationLine(0, 1, 0.07f);
            animationLines[1] = new AnimationLine(1, 2, 0.038f);
            animationLines[2] = new AnimationLine(2, 3, 0.028f);

            animationLines[3] = new AnimationLine(1, 4, 0.04301162633f);
            animationLines[4] = new AnimationLine(4, 5, 0.043f);
            animationLines[5] = new AnimationLine(5, 6, 0.024f);
            animationLines[6] = new AnimationLine(6, 7, 0.02f);

            animationLines[7] = new AnimationLine(4, 8, 0.02549509756f);
            animationLines[8] = new AnimationLine(8, 9, 0.043f);
            animationLines[9] = new AnimationLine(9, 10, 0.03f);
            animationLines[10] = new AnimationLine(10, 11, 0.022f);
            
            animationLines[11] = new AnimationLine(8, 12, 0.02061552812f);
            animationLines[12] = new AnimationLine(12, 13, 0.035f);
            animationLines[13] = new AnimationLine(13, 14, 0.028f);
            animationLines[14] = new AnimationLine(14, 15, 0.025f);

            animationLines[15] = new AnimationLine(12, 16, 0.02121320343f);
            animationLines[16] = new AnimationLine(16, 17, 0.028f);
            animationLines[17] = new AnimationLine(17, 18, 0.02f);
            animationLines[18] = new AnimationLine(18, 19, 0.02f);

            animationLines[19] = new AnimationLine(0, 16, 0.08f);

            for (int i=numOfLines; i < 2 * numOfLines; i++)
            {
                AnimationLine reference = animationLines[i - numOfLines];
                animationLines[i] = new AnimationLine(reference.baseIndex + numOfLines, reference.targetIndex + numOfLines, reference.length);
            }
        }

        private static void initializeKeyPairings()
        {
            char[] firstRow = { 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p' };
            char[] secondRow = {'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';' };
            char[] thirdRow = { 'z', 'x', 'c', 'v', 'b', 'n', 'm' };
            char[] fourthRow = { ' ' };

            float x = 143;
            int y = 185;
            float xSkip = (float)((615 - 143) / 9.0);
            int width = 44;
            int height = 40;

            for (int i = 0; i < firstRow.Length; i++)
            {
                Rectangle rect = new Rectangle((int)x, y, width, height);
                keyPairings[firstRow[i]] = rect;

                x += xSkip;
            }

            x = 156;
            y = 233;

            for (int i=0; i < secondRow.Length; i++)
            {
                Rectangle rect = new Rectangle((int)x, y, width, height);
                keyPairings[secondRow[i]] = rect;

                x += xSkip;
            }

            x = 182;
            y = 279;

            for (int i=0; i < thirdRow.Length; i++)
            {
                Rectangle rect = new Rectangle((int)x, y, width, height);
                keyPairings[thirdRow[i]] = rect;

                x += xSkip;
            }

            x = 288;
            y = 326;
            width = 541 - 288;
            height = 372 - 326;

            for (int i=0; i < fourthRow.Length; i++)
            {
                Rectangle rect = new Rectangle((int)x, y, width, height);
                keyPairings[fourthRow[i]] = rect;

                x += xSkip;
            }
        }


        public static char handleKeyPress(Canvas canvas, CudaVector3 fingerVertex)
        {
            char returnChar = (char)0;

            System.Drawing.Point p = project3Dto2D(fingerVertex, viewport3D);

            foreach (char c in keyPairings.Keys)
            {
                Rectangle regionRect = keyPairings[c];

                if (regionRect.Contains(p.X, p.Y))
                {
                    canvas.Children.Clear();
                    System.Windows.Shapes.Rectangle drawRect = new System.Windows.Shapes.Rectangle();
                    drawRect.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                    drawRect.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                    drawRect.Width = regionRect.Width;
                    drawRect.Height = regionRect.Height;
                    Canvas.SetLeft(drawRect, regionRect.Left);
                    Canvas.SetTop(drawRect, regionRect.Top);
                    canvas.Children.Add(drawRect);

                    returnChar = c;

                    break;
                }
            }

            return returnChar;
        }

        public static void highlightSquare(Canvas canvas, CudaVector3 fingerVertex)
        {
            System.Drawing.Point point = project3Dto2D(fingerVertex, viewport3D);

            canvas.Children.Clear();

            System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
            rect.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
            rect.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
            rect.Width = 40;
            rect.Height = 40;
            Canvas.SetLeft(rect, point.X - 20);
            Canvas.SetTop(rect, point.Y - 20);
            canvas.Children.Add(rect);
        }

        public static void highlightButton(Canvas canvas)
        {
            float x = 143;
            int y = 185;
            float xSkip = (float)((615-143) / 9.0);
            int width = 44;
            int height = 40;
            int numOfKeys = 10;


            for (int i=0; i < numOfKeys; i++)
            {
                System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
                rect.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                rect.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                rect.Width = width;
                rect.Height = height;
                Canvas.SetLeft(rect, x);
                Canvas.SetTop(rect, y);
                canvas.Children.Add(rect);

                x += xSkip;
            }

            x = 156;
            y = 233;
            numOfKeys = 9;
            
            for (int i=0; i < numOfKeys; i++)
            {
                System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
                rect.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                rect.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                rect.Width = width;
                rect.Height = height;
                Canvas.SetLeft(rect, x);
                Canvas.SetTop(rect, y);
                canvas.Children.Add(rect);

                x += xSkip;
            }

            x = 182;
            y = 279;
            numOfKeys = 8;

            for (int i = 0; i < numOfKeys; i++)
            {
                System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
                rect.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                rect.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                rect.Width = width;
                rect.Height = height;
                Canvas.SetLeft(rect, x);
                Canvas.SetTop(rect, y);
                canvas.Children.Add(rect);

                x += xSkip;
            }

            x = 288;
            y = 326;
            width = 541 - 288;
            height = 372 - 326;
            numOfKeys = 1;

            for (int i=0; i < numOfKeys; i++)
            {
                System.Windows.Shapes.Rectangle rect = new System.Windows.Shapes.Rectangle();
                rect.Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                rect.Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));
                rect.Width = width;
                rect.Height = height;
                Canvas.SetLeft(rect, x);
                Canvas.SetTop(rect, y);
                canvas.Children.Add(rect);
            }
        }

        public static Point project3Dto2D(CudaVector3 point3D, Viewport3D viewport)
        {
            double screenX = 0d, screenY = 0d;

            double width = viewport.ActualWidth;
            double height = viewport.ActualHeight;
            PerspectiveCamera cam = viewport.Camera as PerspectiveCamera;

            // Translate input point using camera position
            double inputX = point3D.x - cam.Position.X;
            double inputY = point3D.y - cam.Position.Y;
            double inputZ = point3D.z - cam.Position.Z;

            double aspectRatio = width / height;

            // Apply projection to X and Y
            double tanValue = Math.Tan(Math.PI / 180 * (cam.FieldOfView / 2));
            screenX = inputX / (-inputZ * tanValue);

            screenY = (inputY * aspectRatio) / (-inputZ * tanValue);

            // Convert to screen coordinates
            screenX = width/2 + screenX * width/2;

            screenY = height/2 - height/2 * screenY;

            return new Point((int)screenX, (int)screenY);
        }

        public static void updateModel(CudaVector3[] translations)
        {
            for (int i=0; i < translations.Length; i++)
            {
                CudaVector3 translation = translations[i];

                Transform3DGroup transformGroup = new Transform3DGroup();


                TranslateTransform3D translate = new TranslateTransform3D();
                translate.OffsetX = translation.x;
                translate.OffsetY = translation.y;
                translate.OffsetZ = translation.z;

                transformGroup.Children.Add(translate);

                spheres[i].Transform = transformGroup;
            }

            for (int i=0; i < animationLines.Length; i++)
            {
                Transform3DGroup transformGroup = new Transform3DGroup();

                AnimationLine line = animationLines[i];
                CudaVector3 baseVertex = translations[line.baseIndex];
                CudaVector3 targetVertex = translations[line.targetIndex];

                Vector3D normalizedVector = new Vector3D(targetVertex.x - baseVertex.x, targetVertex.y - baseVertex.y, targetVertex.z - baseVertex.z);
                normalizedVector.Normalize();

                RotateTransform3D rotateTransform = new RotateTransform3D();
                AxisAngleRotation3D axisAngleRotation = new AxisAngleRotation3D();
                axisAngleRotation.Axis = new Vector3D((normalizedVector.X)/2, (normalizedVector.Y) / 2, (normalizedVector.Z + 1) / 2);
                axisAngleRotation.Angle = 180;
                rotateTransform.Rotation = axisAngleRotation;
                

                CudaVector3 translation = translations[animationLines[i].baseIndex];
                TranslateTransform3D translate = new TranslateTransform3D();
                translate.OffsetX = translation.x;
                translate.OffsetY = translation.y;
                translate.OffsetZ = translation.z;

                transformGroup.Children.Add(rotateTransform);
                transformGroup.Children.Add(translate);

                cylinders[i].Transform = transformGroup;
            }
        }

        // Add a cylinder with smooth sides.
        private static void addSmoothCylinder(MeshGeometry3D mesh, Point3D end_point, Vector3D axis, double radius, int num_sides)
        {
            // Get two vectors perpendicular to the axis.
            Vector3D v1;
            if ((axis.Z < -0.01) || (axis.Z > 0.01))
                v1 = new Vector3D(axis.Z, axis.Z, -axis.X - axis.Y);
            else
                v1 = new Vector3D(-axis.Y - axis.Z, axis.X, axis.X);
            Vector3D v2 = Vector3D.CrossProduct(v1, axis);

            // Make the vectors have length radius.
            v1 *= (radius / v1.Length);
            v2 *= (radius / v2.Length);

            // Make the top end cap.
            // Make the end point.
            int pt0 = mesh.Positions.Count; // Index of end_point.
            mesh.Positions.Add(end_point);

            // Make the top points.
            double theta = 0;
            double dtheta = 2 * Math.PI / num_sides;
            for (int i = 0; i < num_sides; i++)
            {
                mesh.Positions.Add(end_point +
                    Math.Cos(theta) * v1 +
                    Math.Sin(theta) * v2);
                theta += dtheta;
            }

            // Make the top triangles.
            int pt1 = mesh.Positions.Count - 1; // Index of last point.
            int pt2 = pt0 + 1;                  // Index of first point in this cap.
            for (int i = 0; i < num_sides; i++)
            {
                mesh.TriangleIndices.Add(pt0);
                mesh.TriangleIndices.Add(pt1);
                mesh.TriangleIndices.Add(pt2);
                pt1 = pt2++;
            }

            // Make the bottom end cap.
            // Make the end point.
            pt0 = mesh.Positions.Count; // Index of end_point2.
            Point3D end_point2 = end_point + axis;
            mesh.Positions.Add(end_point2);

            // Make the bottom points.
            theta = 0;
            for (int i = 0; i < num_sides; i++)
            {
                mesh.Positions.Add(end_point2 +
                    Math.Cos(theta) * v1 +
                    Math.Sin(theta) * v2);
                theta += dtheta;
            }

            // Make the bottom triangles.
            theta = 0;
            pt1 = mesh.Positions.Count - 1; // Index of last point.
            pt2 = pt0 + 1;                  // Index of first point in this cap.
            for (int i = 0; i < num_sides; i++)
            {
                mesh.TriangleIndices.Add(num_sides + 1);    // end_point2
                mesh.TriangleIndices.Add(pt2);
                mesh.TriangleIndices.Add(pt1);
                pt1 = pt2++;
            }

            // Make the sides.
            // Add the points to the mesh.
            int first_side_point = mesh.Positions.Count;
            theta = 0;
            for (int i = 0; i < num_sides; i++)
            {
                Point3D p1 = end_point +
                    Math.Cos(theta) * v1 +
                    Math.Sin(theta) * v2;
                mesh.Positions.Add(p1);
                Point3D p2 = p1 + axis;
                mesh.Positions.Add(p2);
                theta += dtheta;
            }

            // Make the side triangles.
            pt1 = mesh.Positions.Count - 2;
            pt2 = pt1 + 1;
            int pt3 = first_side_point;
            int pt4 = pt3 + 1;
            for (int i = 0; i < num_sides; i++)
            {
                mesh.TriangleIndices.Add(pt1);
                mesh.TriangleIndices.Add(pt2);
                mesh.TriangleIndices.Add(pt4);

                mesh.TriangleIndices.Add(pt1);
                mesh.TriangleIndices.Add(pt4);
                mesh.TriangleIndices.Add(pt3);

                pt1 = pt3;
                pt3 += 2;
                pt2 = pt4;
                pt4 += 2;
            }
        }

        // Add a triangle to the indicated mesh.
        // Reuse points so triangles share normals.
        private static void addSmoothTriangle(MeshGeometry3D mesh, Dictionary<Point3D, int> dict, Point3D point1, Point3D point2, Point3D point3)
        {
            int index1, index2, index3;

            // Find or create the points.
            if (dict.ContainsKey(point1)) index1 = dict[point1];
            else
            {
                index1 = mesh.Positions.Count;
                mesh.Positions.Add(point1);
                dict.Add(point1, index1);
            }

            if (dict.ContainsKey(point2)) index2 = dict[point2];
            else
            {
                index2 = mesh.Positions.Count;
                mesh.Positions.Add(point2);
                dict.Add(point2, index2);
            }

            if (dict.ContainsKey(point3)) index3 = dict[point3];
            else
            {
                index3 = mesh.Positions.Count;
                mesh.Positions.Add(point3);
                dict.Add(point3, index3);
            }

            // If two or more of the points are
            // the same, it's not a triangle.
            if ((index1 == index2) ||
                (index2 == index3) ||
                (index3 == index1)) return;

            // Create the triangle.
            mesh.TriangleIndices.Add(index1);
            mesh.TriangleIndices.Add(index2);
            mesh.TriangleIndices.Add(index3);
        }


        // Add a sphere.
        private static void addSmoothSphere(MeshGeometry3D mesh, Point3D center, double radius, int num_phi, int num_theta)
        {
            // Make a dictionary to track the sphere's points.
            Dictionary<Point3D, int> dict = new Dictionary<Point3D, int>();

            double phi0, theta0;
            double dphi = Math.PI / num_phi;
            double dtheta = 2 * Math.PI / num_theta;

            phi0 = 0;
            double y0 = radius * Math.Cos(phi0);
            double r0 = radius * Math.Sin(phi0);
            for (int i = 0; i < num_phi; i++)
            {
                double phi1 = phi0 + dphi;
                double y1 = radius * Math.Cos(phi1);
                double r1 = radius * Math.Sin(phi1);

                // Point ptAB has phi value A and theta value B.
                // For example, pt01 has phi = phi0 and theta = theta1.
                // Find the points with theta = theta0.
                theta0 = 0;
                Point3D pt00 = new Point3D(
                    center.X + r0 * Math.Cos(theta0),
                    center.Y + y0,
                    center.Z + r0 * Math.Sin(theta0));
                Point3D pt10 = new Point3D(
                    center.X + r1 * Math.Cos(theta0),
                    center.Y + y1,
                    center.Z + r1 * Math.Sin(theta0));
                for (int j = 0; j < num_theta; j++)
                {
                    // Find the points with theta = theta1.
                    double theta1 = theta0 + dtheta;
                    Point3D pt01 = new Point3D(
                        center.X + r0 * Math.Cos(theta1),
                        center.Y + y0,
                        center.Z + r0 * Math.Sin(theta1));
                    Point3D pt11 = new Point3D(
                        center.X + r1 * Math.Cos(theta1),
                        center.Y + y1,
                        center.Z + r1 * Math.Sin(theta1));

                    // Create the triangles.
                    addSmoothTriangle(mesh, dict, pt00, pt11, pt10);
                    addSmoothTriangle(mesh, dict, pt00, pt01, pt11);

                    // Move to the next value of theta.
                    theta0 = theta1;
                    pt00 = pt01;
                    pt10 = pt11;
                }

                // Move to the next value of phi.
                phi0 = phi1;
                y0 = y1;
                r0 = r1;
            }
        }

        private static void defineModel(Model3DGroup modelGroup)
        {

            for (int i=0; i < CudaPSO.numOfVertices * 2; i++)
            {
                MeshGeometry3D mesh1 = new MeshGeometry3D();
                addSmoothSphere(mesh1, new Point3D(0, 0, 0), 0.01, 10, 20);
                SolidColorBrush brush1 = System.Windows.Media.Brushes.Cyan;
                DiffuseMaterial material1 = new DiffuseMaterial(brush1);
                GeometryModel3D model1 = new GeometryModel3D(mesh1, material1);
                modelGroup.Children.Add(model1);
                spheres.Add(model1);
            }

            for (int i=0; i < animationLines.Length; i++)
            {
                MeshGeometry3D mesh5 = new MeshGeometry3D();
                addSmoothCylinder(mesh5, new Point3D(0, 0, 0), new Vector3D(0, 0, animationLines[i].length), 0.005, 10);
                SolidColorBrush brush5 = System.Windows.Media.Brushes.White;
                DiffuseMaterial material5 = new DiffuseMaterial(brush5);
                GeometryModel3D model5 = new GeometryModel3D(mesh5, material5);
                modelGroup.Children.Add(model5);
                cylinders.Add(model5);
            }
        }


        // Define the lights.
        private static void defineLights()
        {
            AmbientLight ambient_light = new AmbientLight(Colors.Gray);
            DirectionalLight directional_light =
                new DirectionalLight(Colors.Gray, new Vector3D(-1.0, -3.0, -2.0));
            mainModel3DGroup.Children.Add(ambient_light);
            mainModel3DGroup.Children.Add(directional_light);
        }

        // Position the camera.
        private static void positionCamera()
        {
            // Calculate the camera's position in Cartesian coordinates.
            //double y = CameraR * Math.Sin(CameraPhi);
            //double hyp = CameraR * Math.Cos(CameraPhi);
            //double x = hyp * Math.Cos(CameraTheta);
            //double z = hyp * Math.Sin(CameraTheta);

            double x = cameraX;
            double y = cameraY;
            double z = cameraZ; 
            camera.Position = new Point3D(x, y, z);

            camera.LookDirection = new Vector3D(0, 0, -z);

            // Set the Up direction.
            camera.UpDirection = new Vector3D(0, 1, 0);

            // Console.WriteLine("Camera.Position: (" + x + ", " + y + ", " + z + ")");
        }
    }
}
