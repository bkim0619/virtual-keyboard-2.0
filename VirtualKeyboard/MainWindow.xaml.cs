﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

using Microsoft.Kinect;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;

using System.Drawing;
using System.IO;
using System.Windows.Media.Media3D;
using static VirtualKeyboard.CudaPSO;

namespace VirtualKeyboard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        bool useKinect = false;
        bool savingToFile = false; // only applicable when using kinect
        bool useCuda = true;
        bool useAnimation = false;

        // Kinect-related
        KinectSensor kinectSensor = null;
        MultiSourceFrameReader multiSourceFrameReader = null;
        ushort[] depthData;
        ushort[] infraredData;
        byte[] depthDataConverted;
        byte[] infraredDataConverted;
        WriteableBitmap bitmap;
        WriteableBitmap bitmap2;
        String debugText;
        String debugText2;
        String debugText3;
        CoordinateMapper coordinateMapper;
        Body[] bodies;
        FrameDescription fd;

        System.Drawing.Point[,] fingerTipHistory;
        System.Drawing.Point[,] wristHistory;
        int[] fingerPressHistory;
        int numOfHistoryPoints = 8;

        CudaVector3[,] fingerVertexHistory;
        CudaVector3[] fingerVertexAveraged;
        int numOfFingerVertexHistory = 4;

        // Emgu-related
        ushort[,,] emguDepthData;
        byte[,,] emguDisplayData;
        Image<Gray, byte> emguDisplayImage;
        Image<Gray, ushort> emguDepthImage;
        Image<Gray, float> emguComputationImage;
        Image<Gray, byte> twoHandsMask;
        Image<Gray, float> twoHandsMasked;
        

        // canvas
        SolidColorBrush red = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 0, 0));
        SolidColorBrush green = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 255, 0));
        SolidColorBrush blue = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 255));

        System.Drawing.Point[] prevWrists;

        public event PropertyChangedEventHandler PropertyChanged;

        [DllImport("user32.dll", SetLastError = true)]
        static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        static extern short VkKeyScan(char ch);

        const int KEYEVENTF_KEYUP = 0x0002;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this; // needed for property event change handler

            if (useCuda)
            {
                CudaPSO.setupCuda();
            }

            if (useAnimation)
            {
                keyboardCanvas.Background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 255, 0, 0));
                Animation.setup(MainViewport);
                //Animation.highlightButton(keyboardCanvas);
            }

            prevWrists = new System.Drawing.Point[2];
            prevWrists[0] = new System.Drawing.Point(0, 0);
            prevWrists[1] = new System.Drawing.Point(0, 0);

            if (!useKinect)
            {
                bitmap = new WriteableBitmap(512, 424, 96.0, 96.0, PixelFormats.Gray8, null);
                depthData = new ushort[512 * 424];
                startFileIndex = 151;
                currFileIndex = 151;
                endFileIndex = 1000;

                float[] accuracies = new float[endFileIndex - startFileIndex + 1];

                for (int i=startFileIndex; i < endFileIndex; i++)
                {
                    accuracies[i-startFileIndex] = testFindWrist(String.Format("tmp{0}.dat", i));
                }

                Debug.WriteLine("Accuracies:");
                for (int i=0; i < accuracies.Length; i++)
                {
                    Debug.Write(accuracies[i] + ",");
                }
                Debug.WriteLine("");

                DebugText2 = String.Format("File{0}", currFileIndex);

            }
            else
            {
                
                fingerVertexHistory = new CudaVector3[numOfFingerVertexHistory, numOfVertices * 2];
                fingerVertexAveraged = new CudaVector3[numOfVertices * 2];

                kinectSensor = KinectSensor.GetDefault();
                coordinateMapper = kinectSensor.CoordinateMapper;

                multiSourceFrameReader = kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Body | FrameSourceTypes.Infrared | FrameSourceTypes.Depth);
                multiSourceFrameReader.MultiSourceFrameArrived += MultiSourceFrameReader_MultiSourceFrameArrived;

                fd = kinectSensor.InfraredFrameSource.FrameDescription;
                depthData = new ushort[fd.LengthInPixels];
                infraredData = new ushort[fd.LengthInPixels];

                depthDataConverted = new byte[fd.LengthInPixels];
                infraredDataConverted = new byte[fd.LengthInPixels];

                emguDepthImage = new Image<Gray, ushort>(fd.Height, fd.Width);
                emguDepthData = new ushort[fd.Height, fd.Width, 1];
                emguDisplayImage = new Image<Gray, byte>(fd.Height, fd.Width);
                emguDisplayData = new byte[fd.Height, fd.Width, 1];

                twoHandsMask = new Image<Gray, byte>(fd.Width, fd.Height);
                twoHandsMasked = new Image<Gray, float>(fd.Width, fd.Height);

                bitmap = new WriteableBitmap(fd.Width, fd.Height, 96.0, 96.0, PixelFormats.Gray8, null);
                bitmap2 = new WriteableBitmap(fd.Width, fd.Height, 96.0, 96.0, PixelFormats.Gray8, null);

                bodies = new Body[6];
                fingerTipHistory = new System.Drawing.Point[numOfFingers * 2, numOfHistoryPoints];
                fingerPressHistory = new int[numOfFingers * 2];
                wristHistory = new System.Drawing.Point[2, numOfHistoryPoints];
                for (int i = 0; i < numOfFingers * 2; i++)
                {
                    fingerPressHistory[i] = 0;
                }

                kinectSensor.Open();
            }
        }

        private void testRotation()
        {
            double xAngle = 45 * 2*Math.PI / 360;
            double yAngle = 90 * 2*Math.PI / 360;
            double zAngle = 90 * 2*Math.PI / 360;

            Vector3 vertex = new Vector3(1, 0, 0);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            Matrix3 rotX = new Matrix3(new Vector3(1, 0, 0), new Vector3(0, (float)Math.Cos(xAngle), (float)-Math.Sin(xAngle)), new Vector3(0, (float)Math.Sin(xAngle), (float)Math.Cos(xAngle)));
            Matrix3 rotY = new Matrix3(new Vector3((float)Math.Cos(yAngle), 0, (float)Math.Sin(yAngle)), new Vector3(0, 1, 0), new Vector3((float)-Math.Sin(yAngle), 0, (float)Math.Cos(yAngle)));
            Matrix3 rotZ = new Matrix3(new Vector3((float)Math.Cos(zAngle), (float)-Math.Sin(zAngle), 0), new Vector3((float)Math.Sin(zAngle), (float)Math.Cos(zAngle), 0), new Vector3(0,0,1));

            Vector3 rotated = rotY.multiply(rotX).multiply(rotZ).multiply(vertex);

            sw.Stop();
            Debug.WriteLine("{0} elapsed", sw.Elapsed);

        }

        private void testQuaternions()
        {
            float xAngle = 45;
            float yAngle = 90;
            float zAngle = 90;

            Vector3 vertex = new Vector3(1, 0, 0);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            Vector3 xAxis = new Vector3(1, 0, 0);
            Vector3 yAxis = new Vector3(0, 1, 0);
            Vector3 zAxis = new Vector3(0, 0, 1);

            sw.Stop();
            Debug.WriteLine("{0} elapsed", sw.Elapsed);
            sw.Restart();

            Quaternion q1 = new Quaternion(xAxis, xAngle);
            Quaternion q2 = new Quaternion(yAxis, yAngle);
            Quaternion q3 = new Quaternion(zAxis, zAngle);

            sw.Stop();
            Debug.WriteLine("{0} elapsed", sw.Elapsed);
            sw.Restart();

            Quaternion q4 = q2.multiply(q1).multiply(q3);

            Vector3 rotated = q4.multiply(vertex);

            sw.Stop();
            Debug.WriteLine("{0} elapsed", sw.Elapsed);

            //Quaternion q2 = new Quaternion(yAxis, yAngle);
            //Quaternion q3 = new Quaternion(zAxis, zAngle);
            //Quaternion q4 = q2.multiply(q1);


            //Debug.WriteLine("x:{0} y:{1} z:{2} w:{3}", q1.v.X, q1.v.Y, q1.v.Z, q1.w);
            //Debug.WriteLine("x:{0} y:{1} z:{2} w:{3}", q2.v.X, q2.v.Y, q2.v.Z, q2.w);
            //Debug.WriteLine("x:{0} y:{1} z:{2} w:{3}", q3.v.X, q3.v.Y, q3.v.Z, q3.w);
            //Debug.WriteLine("x:{0} y:{1} z:{2}", rotatedZAxis.X, rotatedZAxis.Y, rotatedZAxis.Z);
        }

        
        // debug-purpose
        Image<Gray, ushort> image = new Image<Gray, ushort>(512, 424);
        int startFileIndex = 0;
        int currFileIndex = 0;
        int endFileIndex = 0;

        private void separateHand(string path)
        {
            ushort[,,] emguData = readFromFile(path);

            image = new Image<Gray, ushort>(512, 424);
            image.Data = emguData;

            Image<Gray, float> computationImage = image.Convert<Gray, float>();
            Image<Gray, byte> byteImage = image.Convert<Gray, byte>();

            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(byteImage, contours, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            List<int> validContourIndices = new List<int>();

            for (int i = 0; i < contours.Size; i++)
            {
                if (contours[i].Size > 200)
                {
                    validContourIndices.Add(i);
                }
            }

            int leftIndex = validContourIndices[0];
            int rightIndex = validContourIndices[1];

            if (contours[leftIndex][0].X > contours[rightIndex][0].X)
            {
                int tmp = leftIndex;
                leftIndex = rightIndex;
                rightIndex = tmp;
            }

            Rectangle leftRectangle = CvInvoke.BoundingRectangle(contours[leftIndex]);
            Rectangle rightRectangle = CvInvoke.BoundingRectangle(contours[rightIndex]);

            Image<Gray, byte> leftMask = new Image<Gray, byte>(512, 424);
            leftMask.Draw(leftRectangle, new Gray(255), -1);

            Image<Gray, byte> rightMask = new Image<Gray, byte>(512, 424);
            rightMask.Draw(rightRectangle, new Gray(255), -1);

            Image<Gray, ushort> leftImage = new Image<Gray, ushort>(512, 424);
            image.Copy(leftImage, leftMask);

            Image<Gray, ushort> rightImage = new Image<Gray, ushort>(512, 424);
            image.Copy(rightImage, rightMask);

            // save to file
            string leftFilename = path.Substring(0, path.IndexOf('.')) + "_left.dat";
            string rightFilename = path.Substring(0, path.IndexOf('.')) + "_right.dat";
            
            writeToFile(leftImage.Data, leftFilename);
            writeToFile(rightImage.Data, rightFilename);

            Debug.WriteLine("Separated " + path);

            //Image<Gray, byte> display = rightImage.ConvertScale<byte>(255.0 / 8000, 0);

            //bitmap.WritePixels(
            //   new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight),
            //   display.Bytes,
            //   bitmap.PixelWidth,
            //   0);

        }


        private float testFindWrist(string path)
        {
            ushort[,,] emguData = readFromFile(path);

            ushort[] kinectData = new ushort[512 * 424];
            Buffer.BlockCopy(emguData, 0, kinectData, 0, emguData.Length * 2);

            image = new Image<Gray, ushort>(512, 424);
            image.Data = emguData;

            Image<Gray, float> computationImage = image.Convert<Gray, float>();

            //System.Drawing.Size kSize = new System.Drawing.Size(5, 5);
            //CvInvoke.GaussianBlur(computationImage, computationImage, kSize, 3, 3);

            Image<Gray, byte> byteImage = computationImage.Convert<Gray, byte>();
            
            //CvInvoke.Threshold(byteImage, byteImage, 128, -1, ThresholdType.ToZero);

            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(byteImage, contours, null, RetrType.External, ChainApproxMethod.ChainApproxNone);

            List<int> validContourIndices = new List<int>();

            for (int i=0; i < contours.Size; i++)
            {
                if (contours[i].Size > 200)
                {
                    validContourIndices.Add(i);
                }
            }

            if (validContourIndices.Count == 2)
            {
                int leftIndex = validContourIndices[0];
                int rightIndex = validContourIndices[1];

                if (contours[leftIndex][0].X > contours[rightIndex][0].X)
                {
                    int tmp = leftIndex;
                    leftIndex = rightIndex;
                    rightIndex = tmp;
                }

                System.Drawing.Point wristRight = findWrist(contours[rightIndex], computationImage, Handedness.Right);
                System.Drawing.Point wristLeft = findWrist(contours[leftIndex], computationImage, Handedness.Left);

                //CvInvoke.DrawContours(computationImage, contours, rightIndex, new MCvScalar(255 * 255));
                //CvInvoke.DrawContours(computationImage, contours, leftIndex, new MCvScalar(255 * 255));

                //computationImage.Draw(new CircleF(wristRight, 2), new Gray(255 * 255), 1);
                //computationImage.Draw(new CircleF(wristLeft, 2), new Gray(255 * 255), 1);

                List<System.Drawing.Point> rightFingerTips = findFingerTips(contours[rightIndex], computationImage, wristRight, Handedness.Right);
                List<System.Drawing.Point> leftFingerTips = findFingerTips(contours[leftIndex], computationImage, wristLeft, Handedness.Left);

                if (useCuda && rightFingerTips.Count == numOfFingers && leftFingerTips.Count == numOfFingers)
                {
                    int[] cudaLeftFingertips = new int[numOfFingers * 2];
                    int[] cudaRightFingertips = new int[numOfFingers * 2];

                    for (int i = 0; i < numOfFingers; i++)
                    {
                        cudaLeftFingertips[2 * i] = leftFingerTips[i].X;
                        cudaLeftFingertips[2 * i + 1] = leftFingerTips[i].Y;

                        cudaRightFingertips[2 * i] = rightFingerTips[i].X;
                        cudaRightFingertips[2 * i + 1] = rightFingerTips[i].Y;
                    }

                    int wristRightDepth = emguData[wristRight.Y, wristRight.X, 0];
                    int wristLeftDepth = emguData[wristLeft.Y, wristLeft.X, 0];

                    float[] wristRightGlobal = getGlobalCoordinate(wristRight, wristRightDepth);
                    float[] wristLeftGlobal = getGlobalCoordinate(wristLeft, wristLeftDepth);

                    //float[] leftFingertipsGlobal = new float[numOfFingers * 3];
                    //float[] rightFingertipsGlobal = new float[numOfFingers * 3];
                    //for (int i=0; i < numOfFingers; i++)
                    //{
                    //    int leftDepth = emguData[leftFingerTips[i].Y, leftFingerTips[i].X, 0];
                    //    float[] leftGlobal = getGlobalCoordinate(leftFingerTips[i], leftDepth);
                    //    leftFingertipsGlobal[i * 3 + 0] = leftGlobal[0];
                    //    leftFingertipsGlobal[i * 3 + 1] = leftGlobal[1];
                    //    leftFingertipsGlobal[i * 3 + 2] = leftGlobal[2];

                    //    int rightDepth = emguData[rightFingerTips[i].Y, rightFingerTips[i].X, 0];
                    //    float[] rightGlobal = getGlobalCoordinate(rightFingerTips[i], rightDepth);
                    //    rightFingertipsGlobal[i * 3 + 0] = rightGlobal[0];
                    //    rightFingertipsGlobal[i * 3 + 1] = rightGlobal[1];
                    //    rightFingertipsGlobal[i * 3 + 2] = rightGlobal[2];
                    //}

                    CudaVector3[] bestPixels = new CudaVector3[40];

                    float accuracy = 0; 
                    
                    for (int i=0; i < 10; i++)
                    {
                        accuracy += CudaPSO.run(kinectData, kinectData, cudaLeftFingertips, cudaRightFingertips, wristLeftGlobal, wristRightGlobal);
                    }

                   
                    //drawHand(computationImage, bestPixels);
                    
                    if (useAnimation)
                    {
                        Animation.updateModel(bestPixels);

                        //PerspectiveCamera cam = MainViewport.Camera as PerspectiveCamera;
                        //for (int i = 0; i < bestPixels.Length; i++)
                        //{
                        //    System.Drawing.Point p = Animation.project3Dto2D(bestPixels[i], MainViewport);
                        //    System.Windows.Shapes.Ellipse circle = new System.Windows.Shapes.Ellipse() { Width = 30, Height = 30, Fill = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 0, 0)) };
                        //    keyboardCanvas.Children.Add(circle);
                        //    Canvas.SetLeft(circle, p.X - 30 / 2);
                        //    Canvas.SetTop(circle, p.Y - 30 / 2);
                        //}
                    }

                    return (float)Math.Pow((accuracy / 10) / 5, 0.25);
                }

                foreach (System.Drawing.Point fingerTip in rightFingerTips)
                {
                    computationImage.Draw(new CircleF(fingerTip, 2), new Gray(255 * 255), 1);
                }
                foreach (System.Drawing.Point fingerTip in leftFingerTips)
                {
                    computationImage.Draw(new CircleF(fingerTip, 2), new Gray(255 * 255), 1);
                }
            }

            Image<Gray, byte> display = computationImage.ConvertScale<byte>(255.0 / 8000, 0);

            bitmap.WritePixels(
               new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight),
               display.Bytes,
               bitmap.PixelWidth,
               0);

            return 0;
        }

        private float[] getGlobalCoordinate(System.Drawing.Point point, int depth)
        {
            float[] toReturn = new float[3];

            int m = point.X;
            int n = point.Y;

            float fx = 364.5652f;
            float fy = 364.5652f;
            float cx = 257.6964f;
            float cy = 209.9422f;
            float k2 = 0.09627487f;
            float k4 = -0.2699704f;
            float k6 = 0.08848914f;

            float x = (m - cx) / fx;
            float y = (cy - n) / fy;

            float x0 = x;
            float y0 = y;

            for (int i=0; i < 4; i++)
            {
                float r2 = x * x + y * y;
                float p = 1 / (1 + k2 * r2 + k4 * r2 * r2 + k6 * r2 * r2 * r2);
                x = x0 * p;
                y = y0 * p;
            }

            toReturn[0] = x * depth * 0.001f;
            toReturn[1] = y * depth * 0.001f;
            toReturn[2] = depth * 0.001f;

            return toReturn;
        }

        private void testFindFingerTips(string path)
        {
            ushort[,,] emguData = readFromFile(path);
            
            image = new Image<Gray, ushort>(512,424);
            image.Data = emguData;

            Image<Gray, float> computationImage = image.Convert<Gray, float>();
            Image<Gray, byte> byteImage = image.Convert<Gray, byte>();

            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(byteImage, contours, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            Debug.WriteLine("contours size:" + contours.Size);

            List<int> validContourIndices = new List<int>();

            for (int i=0; i < contours.Size; i++)
            {
                if (contours[i].Size > 200)
                {
                    validContourIndices.Add(i);
                }
            }

            Debug.WriteLine("valid contours size:" + validContourIndices.Count);

            int rightIndex = validContourIndices[1];

            Rectangle leftRectangle = CvInvoke.BoundingRectangle(contours[validContourIndices[0]]);
            Rectangle rightRectangle = CvInvoke.BoundingRectangle(contours[validContourIndices[1]]);

            if (leftRectangle.Left > rightRectangle.Left)
            {
                Rectangle tmp = leftRectangle;
                leftRectangle = rightRectangle;
                rightRectangle = tmp;

                rightIndex = validContourIndices[0];
            }

            /*
            List<System.Drawing.Point> fingerTips = findFingerTips(contours[rightIndex], computationImage, Handedness.Right);

            foreach (System.Drawing.Point fingerTip in fingerTips)
            {
                computationImage.Draw(new CircleF(fingerTip, 2), new Gray(255 * 255), 1);
            }
            */

            Image<Gray, byte> display = computationImage.ConvertScale<byte>(255.0 / 8000, 0);

            bitmap.WritePixels(
               new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight),
               display.Bytes,
               bitmap.PixelWidth,
               0);
        }


        Stopwatch sw = new Stopwatch();
        int elapsedCount = 0;
        long elapsedSum = 0;
        float elapsedMax = -1;
        float elapsedMin = 1000000f;

        int[] cudaLeftFingertips = new int[numOfFingers * 2];
        int[] cudaRightFingertips = new int[numOfFingers * 2];

        bool headFound = false;
        DepthSpacePoint headDepthSpacePoint = new DepthSpacePoint();

        private void MultiSourceFrameReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            sw.Restart();

            bool frameProcessed = false;

            MultiSourceFrame multiSourceFrame = e.FrameReference.AcquireFrame();

            if (multiSourceFrame == null)
            {
                return;
            }

            using (DepthFrame depthFrame = multiSourceFrame.DepthFrameReference.AcquireFrame())
            {
                using (InfraredFrame infraredFrame = multiSourceFrame.InfraredFrameReference.AcquireFrame())
                {
                    using (BodyFrame bodyFrame = multiSourceFrame.BodyFrameReference.AcquireFrame())
                    {
                        if (depthFrame != null && infraredFrame != null && bodyFrame != null)
                        {
                            depthFrame.CopyFrameDataToArray(depthData);

                            infraredFrame.CopyFrameDataToArray(infraredData);
                            for (int i = 0; i < infraredData.Length; i++)
                            {
                                byte intensity = (byte)(infraredData[i] >> 8);
                                infraredDataConverted[i] = intensity;
                            }

                            trackingCanvas.Children.Clear();
                            bodyFrame.GetAndRefreshBodyData(bodies);
                            int circleSize = 20;
                            foreach (Body body in bodies)
                            {
                                if (body.IsTracked)
                                {
                                    Joint headJoint = body.Joints[JointType.Head];
                                    if (headJoint.TrackingState == TrackingState.Tracked)
                                    {
                                        headFound = true;
                                        headDepthSpacePoint = coordinateMapper.MapCameraPointToDepthSpace(headJoint.Position);
                                    }

                                    drawCircleOnJointIfTracked(headJoint, circleSize, green);
                                    //drawCircleOnJointIfTracked(body.Joints[JointType.WristRight], circleSize, red);
                                    //drawCircleOnJointIfTracked(body.Joints[JointType.WristLeft], circleSize, blue);
                                }
                            }

                            frameProcessed = true;
                        }
                    }
                }
            }

            if (frameProcessed)
            {
                Buffer.BlockCopy(depthData, 0, emguDepthData, 0, emguDepthData.Length * 2);

                emguDepthImage.Data = emguDepthData;

                emguComputationImage = emguDepthImage.Convert<Gray, float>();

                if (headFound)
                {
                    int headX = (int)(headDepthSpacePoint.X);
                    int headY = (int)(headDepthSpacePoint.Y);
                    ushort headDepth = emguDepthData[headY,headX,0];

                    CvInvoke.Threshold(emguComputationImage, emguComputationImage, headDepth - 400, -1, ThresholdType.ToZero);
                    CvInvoke.Threshold(emguComputationImage, emguComputationImage, headDepth - 150, -1, ThresholdType.ToZeroInv);

                    int twoHandsMaskSize = 300; // TODO: could turn this into xyz coordinate value

                    // make mask
                    twoHandsMask.SetZero();
                    twoHandsMask.Draw(new Rectangle(headX - twoHandsMaskSize / 2, headY - twoHandsMaskSize / 2, twoHandsMaskSize, twoHandsMaskSize), new Gray(255), -1);

                    emguComputationImage.Copy(twoHandsMasked, twoHandsMask);

                    Image<Gray, byte> emguByteImage = twoHandsMasked.Convert<Gray, byte>();

                    //System.Drawing.Size kSize = new System.Drawing.Size(5, 5);
                    //CvInvoke.GaussianBlur(emguByteImage, emguByteImage, kSize, 3, 3);

                    //CvInvoke.Threshold(emguByteImage, emguByteImage, 128, -1, ThresholdType.ToZero);

                    VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
                    CvInvoke.FindContours(emguByteImage, contours, null, RetrType.External, ChainApproxMethod.ChainApproxNone);

                    List<int> handContourIndices = new List<int>();

                    for (int i=0; i < contours.Size; i++)
                    {
                        if (contours[i].Size > 200)
                        {
                            handContourIndices.Add(i);
                        }
                    }

                    if (handContourIndices.Count == 2)
                    {
                        int leftHandIndex = handContourIndices[0];
                        int rightHandIndex = handContourIndices[1];

                        if (contours[leftHandIndex][0].X > contours[rightHandIndex][0].X)
                        {
                            int tmp = leftHandIndex;
                            leftHandIndex = rightHandIndex;
                            rightHandIndex = tmp;
                        }

                        System.Drawing.Point wristLeft = findWrist(contours[leftHandIndex], emguComputationImage, Handedness.Left);
                        System.Drawing.Point wristRight = findWrist(contours[rightHandIndex], emguComputationImage, Handedness.Right);

                        List<System.Drawing.Point> leftFingerTips = findFingerTips(contours[leftHandIndex], emguComputationImage, wristLeft, Handedness.Left);
                        List<System.Drawing.Point> rightFingerTips = findFingerTips(contours[rightHandIndex], emguComputationImage, wristRight, Handedness.Right);

                        frameCount += 1;

                        if (rightFingerTips.Count == numOfFingers && leftFingerTips.Count == numOfFingers)
                        {
                            int historyIndex = correctCount % numOfHistoryPoints;

                            // add to history
                            wristHistory[0, historyIndex] = wristLeft;
                            wristHistory[1, historyIndex] = wristRight;

                            for (int finger=0; finger < numOfFingers; finger++)
                            {
                                System.Drawing.Point leftFingerTip = leftFingerTips[finger];
                                emguComputationImage.Draw(new CircleF(leftFingerTip, 1), new Gray(255 * 255), 1);
                                fingerTipHistory[finger, historyIndex] = leftFingerTip;

                                System.Drawing.Point rightFingerTip = rightFingerTips[finger];
                                emguComputationImage.Draw(new CircleF(rightFingerTip, 1), new Gray(255 * 255), 1);
                                fingerTipHistory[finger + numOfFingers, historyIndex] = rightFingerTip;
                            }

                            correctCount += 1;

                            // check for finger press
                            if (correctCount >= numOfHistoryPoints)
                            {
                                for (int finger=0; finger < numOfFingers*2; finger++)
                                {
                                    int fromFrame = correctCount - numOfHistoryPoints;
                                    int midFrame = correctCount - numOfHistoryPoints / 2;
                                    int toFrame = correctCount - 1;

                                    System.Drawing.Point fromFingerTip = fingerTipHistory[finger, fromFrame % numOfHistoryPoints];
                                    System.Drawing.Point fromWrist = wristHistory[finger / numOfFingers, fromFrame % numOfHistoryPoints];

                                    System.Drawing.Point midFingerTip = fingerTipHistory[finger, midFrame % numOfHistoryPoints];
                                    System.Drawing.Point midWrist = wristHistory[finger / numOfFingers, midFrame % numOfHistoryPoints];

                                    System.Drawing.Point toFingerTip = fingerTipHistory[finger, toFrame % numOfHistoryPoints];
                                    System.Drawing.Point toWrist = wristHistory[finger / numOfFingers, toFrame % numOfHistoryPoints];

                                    float fromDistance = getIntDistanceSquared(fromFingerTip, fromWrist);
                                    float midDistance = getIntDistanceSquared(midFingerTip, midWrist);
                                    float toDistance = getIntDistanceSquared(toFingerTip, toWrist);

                                    float maxDifference = midDistance / fromDistance;

                                    if (maxDifference < 0.8f && (correctCount-1)-fingerPressHistory[finger] > 10)
                                    {
                                        fingerPressHistory[finger] = correctCount-1;

                                        //DebugText3 += String.Format("finger:{0} max:{1};", finger, maxDifference);

                                        if (DebugText3 != null && DebugText3.Length >= 100)
                                        {
                                            //DebugText3 = "";
                                        }

                                        int vertexIndex = -1;
                                        if (finger < numOfFingers)
                                        {
                                            vertexIndex = 3 + finger * 4;
                                        } else
                                        {
                                            vertexIndex = 23 + (finger - numOfFingers) * 4;
                                        }

                                        if (useAnimation)
                                        {
                                            char c = Animation.handleKeyPress(keyboardCanvas, fingerVertexAveraged[vertexIndex]);

                                            sendCharacter(c);

                                            //if (c == ';')
                                            //{
                                            //    DebugText3 = "";
                                            //}
                                            //else if (c != 0)
                                            //{
                                            //    DebugText3 += String.Format("{0}", c);
                                            //}
                                        }

                                    }
                                }
                            }

                            // cuda
                            if (useCuda)
                            {
                                for (int i = 0; i < numOfFingers; i++)
                                {
                                    cudaLeftFingertips[2 * i] = leftFingerTips[i].X;
                                    cudaLeftFingertips[2 * i + 1] = leftFingerTips[i].Y;

                                    cudaRightFingertips[2 * i] = rightFingerTips[i].X;
                                    cudaRightFingertips[2 * i + 1] = rightFingerTips[i].Y;
                                }

                                int wristRightDepth = emguDepthData[wristRight.Y, wristRight.X, 0];
                                int wristLeftDepth = emguDepthData[wristLeft.Y, wristLeft.X, 0];

                                float[] wristRightGlobal = getGlobalCoordinate(wristRight, wristRightDepth);
                                float[] wristLeftGlobal = getGlobalCoordinate(wristLeft, wristLeftDepth);

                                //float[] leftFingertipsGlobal = new float[3 * numOfFingers];
                                //float[] rightFingertipsGlobal = new float[3 * numOfFingers];

                                CudaVector3[] bestPixels = new CudaVector3[40];
                                float foo = CudaPSO.run(depthData, depthData, cudaLeftFingertips, cudaRightFingertips, wristLeftGlobal, wristRightGlobal);

                                // update finger vertex history
                                int fingerHistoryIndex = (correctCount - 1) % numOfFingerVertexHistory;
                                for (int i=0; i < bestPixels.Length; i++)
                                {
                                    fingerVertexHistory[fingerHistoryIndex, i] = bestPixels[i];
                                }

                                if (correctCount >= numOfFingerVertexHistory)
                                {
                                    // average vertices
                                    for (int i=0; i < bestPixels.Length; i++)
                                    {
                                        fingerVertexAveraged[i] = new CudaVector3();

                                        for (int j=0; j < numOfFingerVertexHistory; j++)
                                        {
                                            fingerVertexAveraged[i].add(fingerVertexHistory[j, i]);
                                        }

                                        fingerVertexAveraged[i].divide(numOfFingerVertexHistory);
                                    }
                                }

                                if (useAnimation)
                                {
                                    Animation.updateModel(fingerVertexAveraged);
                                    
                                }
                                //drawHand(emguComputationImage, bestPixels);
                            }
                        }
                        else
                        {
                            missCount += 1;
                        }

                        if (savingToFile && frameCount > 150)
                        {
                            Image<Gray, ushort> saveImage = twoHandsMasked.Convert<Gray, ushort>();
                            writeToFile(saveImage.Data, String.Format("tmp{0}.dat", frameCount));
                            DebugText2 = String.Format("Saved {0}", frameCount);
                        }

                        if (!savingToFile)
                        {
                            //DebugText2 = String.Format("{0}/{1} {2:0.000}", missCount, frameCount, missCount * 1.0 / frameCount);
                        }
                    }
                }

                emguDisplayImage = emguComputationImage.ConvertScale<byte>(255.0 / 8000, 0);
                renderPixels();

                sw.Stop();
                elapsedCount += 1;
                if (sw.ElapsedMilliseconds > elapsedMax)
                {
                    elapsedMax = sw.ElapsedMilliseconds;
                }
                else if (sw.ElapsedMilliseconds < elapsedMin)
                {
                    elapsedMin = sw.ElapsedMilliseconds;
                }

                elapsedSum += sw.ElapsedMilliseconds;
                double average = elapsedSum * 1.0 / elapsedCount;
                DebugText2 = String.Format("avg:{0:0.000} max:{1} min:{2}", average, elapsedMax, elapsedMin);
            }
        }

        int frameCount = 0;
        int correctCount = 0;
        int missCount = 0;

        int getIntDistanceSquared(System.Drawing.Point p1, System.Drawing.Point p2)
        {
            int xDiff = p1.X - p2.X;
            int yDiff = p1.Y - p2.Y;
            return xDiff * xDiff + yDiff * yDiff;
        }

        System.Drawing.Point findWrist2(VectorOfPoint contour, Image<Gray, float> image, Handedness handedness)
        {
            Rectangle rectangle = CvInvoke.BoundingRectangle(contour);

            int bottomThreshold = rectangle.Bottom - rectangle.Height / 6;
            //System.Drawing.Point bottomLeft = new System.Drawing.Point(rectangle.Left, bottomThreshold);
            //System.Drawing.Point bottomRight = new System.Drawing.Point(rectangle.Right, bottomThreshold);

            int[] bottomLeftRight = findLeftRightInImage(image, rectangle.Left, rectangle.Right, bottomThreshold);

            int bottomNeighborThreshold = rectangle.Bottom - rectangle.Height / 5;

            int[] bottomNeighborLeftRight = findLeftRightInImage(image, rectangle.Left, rectangle.Right, bottomNeighborThreshold);

            System.Drawing.Point bottomLeft = new System.Drawing.Point(bottomLeftRight[0], bottomThreshold);
            System.Drawing.Point bottomRight = new System.Drawing.Point(bottomLeftRight[1], bottomThreshold);

            System.Drawing.Point bottomCenter = average(bottomLeft, bottomRight);

            System.Drawing.Point bottomNeighborLeft = new System.Drawing.Point(bottomNeighborLeftRight[0], bottomNeighborThreshold);
            System.Drawing.Point bottomNeighborRight = new System.Drawing.Point(bottomNeighborLeftRight[1], bottomNeighborThreshold);

            System.Drawing.Point bottomNeighborCenter = average(bottomNeighborLeft, bottomNeighborRight);

            // find gradient
            System.Drawing.PointF direction = new System.Drawing.PointF(bottomNeighborCenter.X - bottomCenter.X, bottomNeighborCenter.Y - bottomCenter.Y);

            System.Drawing.PointF curr = bottomNeighborCenter;
            System.Drawing.PointF prev = bottomCenter;

            Debug.WriteLine("hello");

            while (image.Data[(int)curr.Y, (int)curr.X, 0] != 0)
            {
                float currDepth = image.Data[(int)curr.Y, (int)curr.X, 0];
                float prevDepth = image.Data[(int)prev.Y, (int)prev.X, 0];

                float depthDifference = prevDepth - currDepth;
                Debug.WriteLine(depthDifference);

                if (depthDifference < 4)
                {
                    break;
                }

                prev.X = curr.X;
                prev.Y = curr.Y;

                curr.X += direction.X;
                curr.Y += direction.Y;
            }

            drawLines(image, bottomLeft, bottomRight);
            drawLines(image, bottomNeighborLeft, bottomNeighborRight);
            drawLines(image, bottomCenter, bottomNeighborCenter);

            drawLines(image, curr, prev);

            return new System.Drawing.Point((int)prev.X, (int)prev.Y);
        }

        System.Drawing.Point findWrist(VectorOfPoint contour, Image<Gray, float> image, Handedness handedness)
        {
            Rectangle rectangle = CvInvoke.BoundingRectangle(contour);

            int topThreshold = (int)(rectangle.Bottom - rectangle.Height / 1.75);
            System.Drawing.Point topLeft = new System.Drawing.Point(rectangle.Left, topThreshold);
            System.Drawing.Point topRight = new System.Drawing.Point(rectangle.Right, topThreshold);

            int bottomThreshold = rectangle.Bottom - rectangle.Height / 6;
            System.Drawing.Point bottomLeft = new System.Drawing.Point(rectangle.Left, bottomThreshold);
            System.Drawing.Point bottomRight = new System.Drawing.Point(rectangle.Right, bottomThreshold);

            int enteredIndex = 0;
            bool entering = false;

            int numOfWristOutlines = 2;
            int[] wristOutlineIndices = new int[numOfWristOutlines];

            // moving average of points
            List<System.Drawing.Point> averagedPoints = new List<System.Drawing.Point>();
            int averageWindow = 1;
            for (int i = 0; i < contour.Size; i++)
            {
                System.Drawing.Point averaged = new System.Drawing.Point();
                
                if (i >= averageWindow/2 && i < contour.Size - averageWindow/2)
                {
                    for (int j = -averageWindow/2; j <= averageWindow/2; j++)
                    {
                        averaged.X += contour[i + j].X;
                        averaged.Y += contour[i + j].Y;
                    }

                    averaged.X /= averageWindow;
                    averaged.Y /= averageWindow;
                } else
                {
                    averaged.X = contour[i].X;
                    averaged.Y = contour[i].Y;
                }
                averagedPoints.Add(averaged);
                //image.Draw(new CircleF(averaged, 1), new Gray(255 * 255), 1);
            }

            for (int i=0; i < contour.Size; i++)
            {
                System.Drawing.Point point = averagedPoints[i];

                if (!entering)
                {
                    if (point.Y > bottomThreshold)
                    {
                        entering = true;
                        wristOutlineIndices[enteredIndex] = i;

                        //image.Draw(new CircleF(contour[i], 1), new Gray(255 * 255), 1);

                        enteredIndex += 1;
                    }
                } else
                {
                    if (point.Y < bottomThreshold && (i - wristOutlineIndices[enteredIndex-1]) > 10)
                    {
                        entering = false;
                        wristOutlineIndices[enteredIndex] = i-1;
                        //image.Draw(new CircleF(contour[i-1], 1), new Gray(255 * 255), 1);
                        break;
                    }
                }

            }
            
            int w = 0;
            if (handedness == Handedness.Left)
            {
                w = 1;
            }

            int startIndex = wristOutlineIndices[w];
            int[] leftRight = findLeftRightInImage(image, rectangle.Left, rectangle.Right, bottomThreshold);

            System.Drawing.Point startPoint = new System.Drawing.Point(0, bottomThreshold);
            if (handedness == Handedness.Left)
            {
                startPoint.X = leftRight[1];
            } else
            {
                startPoint.X = leftRight[0];
            }

            float cosineThreshold = -0.9f; // -0.93f

            int maxIndex = startIndex;

            int direction = handedness == Handedness.Left ? 1 : -1;

            int neighborDistance = 10;
            int neighborDistanceSquared = neighborDistance * neighborDistance;

            for (int i = startIndex + direction; i >= 0 && i < contour.Size; i += direction)
            {
                Vector vector1 = new Vector(0, 0);
                Vector vector2 = new Vector(0, 0);

                if (handedness == Handedness.Left)
                {
                    vector1 = new Vector(averagedPoints[i], startPoint);  
                    if (vector1.magnitude < neighborDistance)
                    {
                        continue;
                    }
                } else
                {
                    vector2 = new Vector(averagedPoints[i], startPoint);
                    if (vector2.magnitude < neighborDistance)
                    {
                        continue;
                    }
                }

                // find neighbor far enough
                int neighborIndex = i + direction;
                while (neighborIndex >= 0 && neighborIndex < contour.Size)
                {
                    int squaredDistance = (int)getDistanceSquared(averagedPoints[i], averagedPoints[neighborIndex]);
                    if (squaredDistance > neighborDistanceSquared)
                    {
                        break;
                    }
                    neighborIndex += direction;
                }

                if (neighborIndex < 0 || neighborIndex >= contour.Size)
                {
                    break;
                }

                if (handedness == Handedness.Left)
                {
                    vector2 = new Vector(averagedPoints[i], averagedPoints[neighborIndex]);
                } else
                {
                    vector1 = new Vector(averagedPoints[i], averagedPoints[neighborIndex]);
                }

                if (vector1.perpendicularDotProduct(vector2) < 0) // less than zero because of y-axis flip in emgu
                {
                    float cosineAngle = vector1.cosineAngle(vector2);

                    if (cosineAngle > cosineThreshold)
                    {
                        maxIndex = i;

                        if (handedness == Handedness.Left)
                        {
                            //image.Draw(new CircleF(startPoint, 1), new Gray(255 * 255), 2);
                            //image.Draw(new CircleF(averagedPoints[neighborIndex], 1), new Gray(255 * 255), 1);
                        } else
                        {
                            //image.Draw(new CircleF(startPoint, 1), new Gray(255 * 255), 2);
                            //image.Draw(new CircleF(averagedPoints[neighborIndex], 1), new Gray(255 * 255), 1);
                        }

                        break;
                    }
                }
            }

            int wristIndex = maxIndex;

            System.Drawing.Point wristSide = averagedPoints[wristIndex];
            //image.Draw(new CircleF(wristSide, 1), new Gray(255 * 255), 1);

            int x = wristSide.X;
            int y = wristSide.Y;

            int increment = handedness == Handedness.Left ? -1 : 1;

            while (image.Data[y,x,0] > 0)
            {
                x += increment;
            }

            x -= increment;

            System.Drawing.Point wristOppositeSide = new System.Drawing.Point(x, y);
            //image.Draw(new CircleF(wristOppositeSide, 1), new Gray(255 * 255), 1);

            System.Drawing.Point wrist = average(wristSide, wristOppositeSide);

            int prevWristIndex = 0;
            if (handedness == Handedness.Left)
            {
                prevWristIndex = 0;
            } else
            {
                prevWristIndex = 1;
            }

            if (prevWrists[prevWristIndex].X == 0 && prevWrists[prevWristIndex].Y == 0)
            {
                prevWrists[prevWristIndex] = wrist;
            } else
            {
                if (getDistanceSquared(prevWrists[prevWristIndex], wrist) > 400)
                {
                    wrist = prevWrists[prevWristIndex];
                } else
                {
                    prevWrists[prevWristIndex] = wrist;
                }
            }

            image.Draw(new CircleF(wrist, 2), new Gray(255 * 255), 1);

            //drawLines(image, bottomRight, bottomLeft);
            //drawLines(image, topRight, topLeft);

            return wrist;
        }

        int[] findLeftRightInImage(Image<Gray, float> image, int x1, int x2, int y)
        {
            int[] leftRight = new int[2];

            int left = x1;
            while (left < 512 && image.Data[y, left, 0] == 0)
            {
                left += 1;
            }

            int right = x2;
            while (right >= 0 && image.Data[y, right,0] == 0)
            {
                right -= 1;
            }

            leftRight[0] = left;
            leftRight[1] = right;

            return leftRight;
        }

        List<System.Drawing.Point> findFingerTips(VectorOfPoint contour, Image<Gray, float> image, System.Drawing.Point wrist, Handedness handedness)
        {
            int comparisonInterval = 3; // 3, index difference between points, determines extreme points
            int contendingInterval = 25; // 25, 6, between each potential fingertip, determines fingertip points

            //RotatedRect rotatedRectangle = CvInvoke.MinAreaRect(contour);
            //image.Draw(rotatedRectangle, new Gray(255 * 255), 1);

            Rectangle rectangle = CvInvoke.BoundingRectangle(contour);

            System.Drawing.Point topLeft = new System.Drawing.Point(rectangle.Left, rectangle.Top);
            System.Drawing.Point topRight = new System.Drawing.Point(rectangle.Right, rectangle.Top);
            System.Drawing.Point bottomLeft = new System.Drawing.Point(rectangle.Left, rectangle.Bottom);
            System.Drawing.Point bottomRight = new System.Drawing.Point(rectangle.Right, rectangle.Bottom);
            System.Drawing.Point center = new System.Drawing.Point((rectangle.Left + rectangle.Right) / 2, (rectangle.Top + rectangle.Bottom) / 2);

            //System.Drawing.PointF[] vertices = rotatedRectangle.GetVertices(); // 0, 1, 2, 3 clock-wise, 0 is bottom left corner

            //System.Drawing.Point wrist = new System.Drawing.Point((rectangle.Left + rectangle.Right) / 2, rectangle.Bottom);
            
            //System.Drawing.Point wrist = new System.Drawing.Point((int)(vertices[0].X + vertices[3].X) / 2, (int)(vertices[0].Y + vertices[3].Y) / 2);

            List<float> squaredDistances = new List<float>();

            for (int i=0; i < contour.Size; i++)
            {
                System.Drawing.Point contourPoint = contour[i];
                float squaredDistance = getDistanceSquared(contourPoint, wrist);
                squaredDistances.Add(squaredDistance);
            }

            List<int> extremes = new List<int>();
            for (int i=0; i < squaredDistances.Count; i++)
            {
                int leftIndex = i - comparisonInterval;
                if (leftIndex < 0)
                {
                    leftIndex += squaredDistances.Count;
                }
                int rightIndex = i + comparisonInterval;
                if (rightIndex >= squaredDistances.Count)
                {
                    rightIndex -= squaredDistances.Count;
                }

                if (squaredDistances[i] > squaredDistances[leftIndex] && squaredDistances[i] > squaredDistances[rightIndex]) // local extreme
                {
                    if (extremes.Count == 0)
                    {
                        extremes.Add(i);
                    } else // need to deal with contending neighbors
                    {
                        int prevIndex = extremes[extremes.Count - 1];

                        if ((i - prevIndex) > contendingInterval)
                        {
                            extremes.Add(i);
                        }
                        else
                        {
                            if (squaredDistances[i] > squaredDistances[prevIndex])
                            {
                                extremes[extremes.Count - 1] = i;
                            }
                        }
                    }
                }
            }

            int firstIndex = extremes[0];
            int lastIndex = extremes[extremes.Count - 1];

            if ((firstIndex - lastIndex + contour.Size) < contendingInterval) // if first and last extreme points are too close
            {
                if (squaredDistances[firstIndex] > squaredDistances[lastIndex])
                {
                    extremes.RemoveAt(extremes.Count - 1);
                }
                else
                {
                    extremes.RemoveAt(0);
                }
            }

            List<int> fingerTips = new List<int>();
            
            // retain only upper points
            //System.Drawing.PointF leftHalf = new System.Drawing.PointF((vertices[0].X + vertices[1].X) / 2, (vertices[0].Y + vertices[1].Y) / 2);
            //System.Drawing.PointF rightHalf = new System.Drawing.PointF((vertices[2].X + vertices[3].X) / 2, (vertices[2].Y + vertices[3].Y) / 2);

            //int yThreshold = (int)(rectangle.Top + (rectangle.Bottom - rectangle.Top) * upperRatioToSave);

            System.Drawing.PointF leftHalf = new System.Drawing.Point(rectangle.Left, rectangle.Top + rectangle.Height / 2);
            System.Drawing.PointF rightHalf = new System.Drawing.Point(rectangle.Right, rectangle.Top + rectangle.Height / 2);

            for (int i=0; i < extremes.Count; i++)
            {
                int pointIndex = extremes[i];
                System.Drawing.Point potential = contour[pointIndex];
                if (isPointInRectangle(potential, leftHalf, topLeft, rightHalf))
                {
                    fingerTips.Add(pointIndex);
                }
            }

            // sort by x
            if (handedness == Handedness.Left)
            {
                fingerTips.Sort((a, b) => contour[b].X - contour[a].X);
            }
            else
            {
                fingerTips.Sort((a, b) => contour[a].X - contour[b].X);
            }

            // extra logic to eliminate weird points near thumb
            List<int> midPoints = new List<int>();

            //int lowerThird = rectangle.Top + (yThreshold - rectangle.Top) / 3;
            //int rightHalf = rectangle.Left + rectangle.Width / 2;
            float divisionFactor = 2.8f;
            System.Drawing.PointF leftQuarter = new System.Drawing.PointF(rectangle.Left, rectangle.Top + (rectangle.Height/2) / divisionFactor);
            System.Drawing.PointF rightQuarter = new System.Drawing.PointF(rectangle.Right, rectangle.Top + (rectangle.Height/2) / divisionFactor);
            System.Drawing.PointF centerQuarter = new System.Drawing.PointF((leftQuarter.X + rightQuarter.X) / 2, leftQuarter.Y);

            System.Drawing.PointF half; // could be left half or right half
            System.Drawing.PointF quarter; // could be left "quarter" or right "quarter"

            if (handedness == Handedness.Right)
            {
                half = leftHalf;
                quarter = leftQuarter;
            } else
            {
                half = rightHalf;
                quarter = rightQuarter;
            }

            //drawLines(image, center, centerQuarter, quarter, half);

            for (int i=0; i < fingerTips.Count; i++)
            {
                if (isPointInRectangle(contour[fingerTips[i]], center, centerQuarter, half))
                {
                    midPoints.Add(i);
                }
            }

            if (midPoints.Count > 1)
            {
                for (int i=midPoints.Count-1; i > 0; i--)
                {
                    fingerTips.RemoveAt(midPoints[i]);
                }
            }

            List<int> finalFingerTips = new List<int>();
            if (fingerTips.Count > 0)
            {
                finalFingerTips.Add(fingerTips[0]);
            }
            

            // sort by y
            fingerTips.Sort((a, b) => contour[a].Y - contour[b].Y);
            if (fingerTips.Count > numOfFingers-1)
            {
                for (int i = 0; i < numOfFingers - 1; i++)
                {
                    finalFingerTips.Add(fingerTips[i]);
                }
            }
            
            // sort by x again
            if (handedness == Handedness.Left)
            {
                finalFingerTips.Sort((a, b) => contour[b].X - contour[a].X);
            }
            else
            {
                finalFingerTips.Sort((a, b) => contour[a].X - contour[b].X);
            }


            //foreach (System.Drawing.Point tip in fingerTips)
            //{
            //    image.Draw(new CircleF(tip, 1), new Gray(255 * 255), 1);
            //}

            //drawLines(image, bottomLeft, topLeft, topRight, bottomRight);
            //drawLines(image, leftHalf, rightHalf);

            //fingerTips.Add(wrist);            

            List<System.Drawing.Point> toReturn = new List<System.Drawing.Point>();
            for (int i=0; i < finalFingerTips.Count; i++)
            {
                toReturn.Add(contour[finalFingerTips[i]]);
            }

            return toReturn;
        }

        System.Drawing.Point getNormal(System.Drawing.Point center, System.Drawing.Point p1, System.Drawing.Point p2)
        {
            System.Drawing.Point v1 = new System.Drawing.Point();
            v1.X = p1.X - center.X;
            v1.Y = p1.Y - center.Y;

            System.Drawing.Point v2 = new System.Drawing.Point();
            v2.X = p2.X - center.X;
            v2.Y = p2.Y - center.Y;

            System.Drawing.Point toReturn = new System.Drawing.Point();
            toReturn.X = (10*(v1.X + v2.X) / 2 + center.X);
            toReturn.Y = (10*(v1.Y + v2.Y) / 2 + center.Y);
            return toReturn;
        }

        void drawHand(Image<Gray, float> image, CudaVector3[] bothPixels)
        {
            for (int i = 0; i < bothPixels.Length; i++)
            {
                System.Drawing.Point p = new System.Drawing.Point((int)bothPixels[i].x, (int)bothPixels[i].y);

                image.Draw(new CircleF(p, 2), new Gray(255 * 255), -1);
            }

            for (int i=0; i < 2; i++)
            {
                int offset = i * numOfVertices;

                for (int v = 1; v < numOfVertices; v++)
                {
                    System.Drawing.Point p1, p2;

                    p2 = new System.Drawing.Point((int)bothPixels[offset + v].x, (int)bothPixels[offset + v].y);

                    if (v == 1 || v == 4 || v == 8 || v == 12 || v == 16)
                    {
                        p1 = new System.Drawing.Point((int)bothPixels[offset].x, (int)bothPixels[offset].y);
                        
                    } else 
                    {
                        p1 = new System.Drawing.Point((int)bothPixels[offset + v - 1].x, (int)bothPixels[offset + v - 1].y);
                    }
                    
                    image.Draw(new LineSegment2D(p1, p2), new Gray(255 * 255), 1);
                }
            }
        }

        void drawLines(Image<Gray, float> image, params System.Drawing.PointF[] pointFs)
        {
            System.Drawing.Point[] points = new System.Drawing.Point[pointFs.Length]; // convert to points
            for (int i=0; i < pointFs.Length; i++)
            {
                points[i] = new System.Drawing.Point((int)pointFs[i].X, (int)pointFs[i].Y);
            }

            for (int i=0; i < points.Length; i++)
            {
                System.Drawing.Point a, b;
                a = points[i];
                if (i==points.Length-1)
                {
                    if (i==1) // no need to complete figure if only two points
                    {
                        break;
                    }

                    b = points[0];
                } else
                {
                    b = points[i + 1];
                }

                image.Draw(new LineSegment2D(a, b), new Gray(255 * 255), 1);
            }
        }

        public System.Drawing.Point average(System.Drawing.Point a, System.Drawing.Point b)
        {
            return new System.Drawing.Point((a.X + b.X) / 2, (a.Y + b.Y) / 2);
        }

        public class Vector {
            public float x;
            public float y;
            public float magnitude;

            public Vector(float x, float y)
            {
                this.x = x;
                this.y = y;
                this.magnitude = getMagnitude();
            }

            public Vector(System.Drawing.Point origin, System.Drawing.Point target)
            {
                this.x = target.X - origin.X;
                this.y = target.Y - origin.Y;
                this.magnitude = getMagnitude();
            }

            public float dot(Vector b)
            {
                return x * b.x + y * b.y;
            }

            public float perpendicularDotProduct(Vector b) // if result is < 0, then vectors are more than 180 degrees apart counter-clockwise
            {
                return x * b.y - y * b.x;
            }

            public float getMagnitude()
            {
                return (float)Math.Sqrt(x * x + y * y);
            }

            public float cosineAngle(Vector b)
            {
                return dot(b) / (magnitude * b.magnitude);
            }

            public void rotateClockwiseBy90()
            {
                float tmp = x;
                x = y;
                y = -tmp;
            }
        }


        bool isPointInRectangle(System.Drawing.Point m, System.Drawing.PointF a, System.Drawing.PointF b, System.Drawing.PointF d)
        {
            // (0 < AM.AB < AB.AB) and (0 < AM.AD < AD.AD)

            Vector AM = new Vector(m.X - a.X, m.Y - a.Y);
            Vector AB = new Vector(b.X - a.X, b.Y - a.Y);
            Vector AD = new Vector(d.X - a.X, d.Y - a.Y);

            return (0 <= AM.dot(AB) && AM.dot(AB) <= AB.dot(AB) && 0 <= AM.dot(AD) && AM.dot(AD) <= AD.dot(AD));
        }


        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            string str = e.Key.ToString();

            switch (str) {
                case "Right":
                    currFileIndex += 1;
                    if (currFileIndex > endFileIndex)
                    {
                        currFileIndex = startFileIndex;
                    }
                    testFindWrist(String.Format("tmp{0}.dat", currFileIndex));
                    break;
                case "Left":
                    currFileIndex -= 1;
                    if (currFileIndex < startFileIndex)
                    {
                        currFileIndex = endFileIndex;
                    }
                    testFindWrist(String.Format("tmp{0}.dat", currFileIndex));
                    break;
            }
            DebugText2 = String.Format("File{0}", currFileIndex);
        }

        public ImageSource ImageSource
        {
            get
            {
                return bitmap;
            }
        }

        public ImageSource ImageSource2
        {
            get
            {
                return bitmap2;
            }
        }

        public String DebugText
        {
            get
            {
                return debugText;
            }
            set
            {
                if (debugText != value)
                {
                    debugText = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("DebugText"));
                    }
                }
            }
        }

        public String DebugText2
        {
            get
            {
                return debugText2;
            }
            set
            {
                if (debugText2 != value)
                {
                    debugText2 = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("DebugText2"));
                    }
                }
            }
        }

        public String DebugText3
        {
            get
            {
                return debugText3;
            }
            set
            {
                if (debugText3 != value)
                {
                    debugText3 = value;

                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("DebugText3"));
                    }
                }
            }
        }

        /// <summary>
        /// Used for sending keyboard press to other applications
        /// </summary>
        /// <param name="c"></param>
        private void sendCharacter(char c)
        {
            keybd_event((byte)VkKeyScan(c), 0x9e, 0, 0); // press
            keybd_event((byte)VkKeyScan(c), 0x9e, KEYEVENTF_KEYUP, 0); // release
        }

        private float getDistanceSquared(System.Drawing.Point a, System.Drawing.Point b) {
            float distance = (float)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
            return distance;
        }

        private void drawCircleOnJointIfTracked(Joint joint, int circleSize, SolidColorBrush color)
        {
            System.Windows.Shapes.Ellipse circle = new System.Windows.Shapes.Ellipse() { Width = circleSize, Height = circleSize, Fill = color };
            trackingCanvas.Children.Add(circle);
            DepthSpacePoint dsp = coordinateMapper.MapCameraPointToDepthSpace(joint.Position);
            Canvas.SetLeft(circle, dsp.X - circleSize / 2);
            Canvas.SetTop(circle, dsp.Y - circleSize / 2);
        }

        private void renderPixels()
        {
            bitmap.WritePixels(
                new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight),
                infraredDataConverted,
                bitmap.PixelWidth,
                0);

            bitmap2.WritePixels(
                new Int32Rect(0, 0, bitmap2.PixelWidth, bitmap2.PixelHeight),
                emguDisplayImage.Bytes,
                bitmap2.PixelWidth,
                0);
        }

        private void Image_MouseMove2(object sender, MouseEventArgs e)
        {
            System.Windows.Point point = e.GetPosition((System.Windows.Controls.Canvas)sender);

            int x = (int)point.X;
           
            int y = (int)point.Y;

            DebugText3 = String.Format("XY: {0} {1}", x, y);
        }

        private void Image_MouseMove(object sender, MouseEventArgs e)
        {
            System.Windows.Point point = e.GetPosition((System.Windows.Controls.Image)sender);

            int x = (int)point.X;
            if (x >= 512)
            {
                x = 511;
            }
            int y = (int)point.Y;
            if (y >= 424)
            {
                y = 423;
            }

            ushort depth = depthData[512 * y + x];

            if (!useKinect)
            {
                DebugText = String.Format("XY:{0},{1} depth:{2}", x, y, image.Data[y, x, 0]);
            } else
            {
                CameraIntrinsics intrinsics = coordinateMapper.GetDepthCameraIntrinsics();

                DepthSpacePoint depthSpacePoint = new DepthSpacePoint();
                depthSpacePoint.X = x;
                depthSpacePoint.Y = y;
                CameraSpacePoint cameraSpacePoint = coordinateMapper.MapDepthPointToCameraSpace(depthSpacePoint, depth);
                //DebugText = String.Format("XY:{0},{1} depth:{2} XYZ:{3:0.00},{4:0.00},{5:0.00} table:{6} {7}", x, y, depth, cameraSpacePoint.X, cameraSpacePoint.Y, cameraSpacePoint.Z, foo[512 * y + x].X, foo[512 * y + x].Y);
                DebugText = String.Format("{0} {1} {2} {3} {4} {5} {6}", intrinsics.FocalLengthX, intrinsics.FocalLengthY, intrinsics.PrincipalPointX, intrinsics.PrincipalPointY, intrinsics.RadialDistortionSecondOrder, intrinsics.RadialDistortionFourthOrder, intrinsics.RadialDistortionSixthOrder);
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (useCuda)
            {
                CudaPSO.end();
            }

            if (multiSourceFrameReader != null)
            {
                multiSourceFrameReader.Dispose();
                multiSourceFrameReader = null;
            }

            if (kinectSensor != null)
            {
                kinectSensor.Close();
                kinectSensor = null;
            }
        }

        ushort[,,] readFromFile(string path)
        {
            ushort[] data;

            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                data = new ushort[fs.Length / 2];

                using (BinaryReader br = new BinaryReader(fs))
                {
                    for (int i = 0; i < data.Length; i++)
                    {
                        data[i] = br.ReadUInt16();
                    }
                }
            }

            ushort[,,] emguData = new ushort[424, 512, 1];
            Buffer.BlockCopy(data, 0, emguData, 0, data.Length * 2);

            return emguData;
        }
        private void writeToFile(ushort[,,] values, string path)
        {
            ushort[] data = new ushort[values.Length];
            Buffer.BlockCopy(values, 0, data, 0, values.Length * 2);
            writeToFile(data, path);
        }
        private void writeToFile(ushort[] values, string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    foreach (short value in values)
                    {
                        bw.Write(value);
                    }
                }
            }
        }

        public enum Handedness {
            Left,
            Right
        }


        /// <summary>
        /// Delete a GDI object
        /// </summary>
        /// <param name="o">The pointer to the GDI object to be deleted</param>
        /// <returns></returns>
        [DllImport("gdi32")]
        private static extern int DeleteObject(IntPtr o);

        /// <summary>
        /// Convert an IImage to a WPF BitmapSource. The result can be used in the Set Property of Image.Source
        /// </summary>
        /// <param name="image">The Emgu CV Image</param>
        /// <returns>The equivalent BitmapSource</returns>
        public static BitmapSource ToBitmapSource(IImage image)
        {
            using (System.Drawing.Bitmap source = image.Bitmap)
            {
                IntPtr ptr = source.GetHbitmap(); //obtain the Hbitmap

                BitmapSource bs = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    ptr,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

                DeleteObject(ptr); //release the HBitmap
                return bs;
            }
        }
    }
}
