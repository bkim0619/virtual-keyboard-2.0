#include <curand_kernel.h>
struct CudaPSOCudaVector3
{
	__device__  CudaPSOCudaVector3()
	{
	}
	float x;
	float y;
	float z;
	__device__  float dot(CudaPSOCudaVector3 q)
	{
		return x * q.x + y * q.y + z * q.z;
	}
	__device__  void subtract(CudaPSOCudaVector3 q)
	{
		x -= q.x;
		y -= q.y;
		z -= q.z;
	}
	__device__  void add(CudaPSOCudaVector3 q)
	{
		x += q.x;
		y += q.y;
		z += q.z;
	}
	__device__  void divide(float r)
	{
		x /= r;
		y /= r;
		z /= r;
	}
	__device__  void rotate(CudaPSOCudaVector3 r0, CudaPSOCudaVector3 r1, CudaPSOCudaVector3 r2)
	{
		float num = r0.x * x + r0.y * y + r0.z * z;
		float num2 = r1.x * x + r1.y * y + r1.z * z;
		float num3 = r2.x * x + r2.y * y + r2.z * z;
		x = num;
		y = num2;
		z = num3;
	}
};

struct CudaPSOCudaLine
{
	__device__  CudaPSOCudaLine()
	{
	}
	int vertexIndex0;
	int vertexIndex1;
};

struct CudaPSOCudaJoint
{
	__device__  CudaPSOCudaJoint()
	{
	}
	int isIdentity;
	CudaPSOCudaVector3 translation;
	CudaPSOCudaVector3 axis;
	int dimensionIndex;
};

struct CudaPSOCudaVertex
{
	__device__  CudaPSOCudaVertex()
	{
	}
	CudaPSOCudaVector3 position;
	CudaPSOCudaJoint joint0;
	CudaPSOCudaJoint joint1;
	CudaPSOCudaJoint joint2;
	CudaPSOCudaJoint joint3;
};


// VirtualKeyboard.CudaPSO
extern "C" __global__  void setupRandom( curandStateXORWOW* state, int stateLen0, unsigned long long seed);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void testRandom( float* currPositions, int currPositionsLen0,  curandStateXORWOW* state, int stateLen0);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void trackGlobalBest( float* gBestFitness, int gBestFitnessLen0,  float* globalBestTrend, int globalBestTrendLen0, int iteration);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void initialize( float* currPositions, int currPositionsLen0,  float* currVelocities, int currVelocitiesLen0,  float* pBestFitness, int pBestFitnessLen0,  float* gBestFitness, int gBestFitnessLen0,  curandStateXORWOW* state, int stateLen0,  float* fixedPositions, int fixedPositionsLen0, int handStream);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void rotateVertices( float* positions, int positionsLen0,  CudaPSOCudaVector3* rotatedVertices, int rotatedVerticesLen0, int handStream);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void computePixels( CudaPSOCudaVector3* rotatedVertices, int rotatedVerticesLen0,  CudaPSOCudaVector3* pixels, int pixelsLen0);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void computeFitness( float* positions, int positionsLen0,  CudaPSOCudaVector3* rotatedVertices, int rotatedVerticesLen0,  float* pBestFitness, int pBestFitnessLen0,  float* pBestPositions, int pBestPositionsLen0,  unsigned short* depthImage, int depthImageLen0,  int* fingerTips, int fingerTipsLen0);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void computeFitness2( float* positions, int positionsLen0,  float* pBestFitness, int pBestFitnessLen0,  float* pBestPositions, int pBestPositionsLen0);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void findLocalBest( float* pBestFitness, int pBestFitnessLen0,  int* pBestIndex, int pBestIndexLen0);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void updateGlobalBest( float* pBestPositions, int pBestPositionsLen0,  float* gBestPositions, int gBestPositionsLen0,  float* pBestFitness, int pBestFitnessLen0,  float* gBestFitness, int gBestFitnessLen0,  int* pBestIndex, int pBestIndexLen0);
// VirtualKeyboard.CudaPSO
extern "C" __global__  void updateParticlePosition( float* positions, int positionsLen0,  float* velocities, int velocitiesLen0,  float* pBestPositions, int pBestPositionsLen0,  float* gBestPositions, int gBestPositionsLen0,  curandStateXORWOW* state, int stateLen0, int handStream);
// VirtualKeyboard.CudaPSO
__device__  float f1(float x);

// VirtualKeyboard.CudaPSO
__constant__ float dev_rightLowerLimits[25];
#define dev_rightLowerLimitsLen0 25
// VirtualKeyboard.CudaPSO
__constant__ float dev_rightUpperLimits[25];
#define dev_rightUpperLimitsLen0 25
// VirtualKeyboard.CudaPSO
__constant__ float dev_leftLowerLimits[25];
#define dev_leftLowerLimitsLen0 25
// VirtualKeyboard.CudaPSO
__constant__ float dev_leftUpperLimits[25];
#define dev_leftUpperLimitsLen0 25
// VirtualKeyboard.CudaPSO
__constant__ CudaPSOCudaVertex dev_rightHandVertices[20];
#define dev_rightHandVerticesLen0 20
// VirtualKeyboard.CudaPSO
__constant__ CudaPSOCudaVertex dev_leftHandVertices[20];
#define dev_leftHandVerticesLen0 20
// VirtualKeyboard.CudaPSO
__constant__ CudaPSOCudaLine dev_lines[19];
#define dev_linesLen0 19
// VirtualKeyboard.CudaPSO
extern "C" __global__  void setupRandom( curandStateXORWOW* state, int stateLen0, unsigned long long seed)
{
	int num = threadIdx.x + blockIdx.x * 25;
	curand_init(seed, (unsigned long long)((long long)num), 0uL, &state[(num)]);
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void testRandom( float* currPositions, int currPositionsLen0,  curandStateXORWOW* state, int stateLen0)
{
	int num = threadIdx.x + blockIdx.x * 25;
	curandStateXORWOW randStateXORWOW = state[(num)];
	currPositions[(num)] = curand_uniform(&randStateXORWOW);
	state[(num)] = randStateXORWOW;
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void trackGlobalBest( float* gBestFitness, int gBestFitnessLen0,  float* globalBestTrend, int globalBestTrendLen0, int iteration)
{
	globalBestTrend[(iteration)] = gBestFitness[(0)];
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void initialize( float* currPositions, int currPositionsLen0,  float* currVelocities, int currVelocitiesLen0,  float* pBestFitness, int pBestFitnessLen0,  float* gBestFitness, int gBestFitnessLen0,  curandStateXORWOW* state, int stateLen0,  float* fixedPositions, int fixedPositionsLen0, int handStream)
{
	int x = blockIdx.x;
	int x2 = threadIdx.x;
	int num = x * 25 + x2;
	curandStateXORWOW randStateXORWOW = state[(num)];
	float num2 = curand_uniform(&randStateXORWOW);
	state[(num)] = randStateXORWOW;
	bool flag = handStream == 0;
	float num3;
	float num4;
	if (flag)
	{
		num3 = dev_leftLowerLimits[(x2)];
		num4 = dev_leftUpperLimits[(x2)];
	}
	else
	{
		num3 = dev_rightLowerLimits[(x2)];
		num4 = dev_rightUpperLimits[(x2)];
	}
	bool flag2 = x2 < 3;
	if (flag2)
	{
		currPositions[(num)] = fixedPositions[(x2)];
	}
	else
	{
		currPositions[(num)] = num3 + (num4 - num3) * num2;
	}
	currVelocities[(num)] = 0.0f;
	bool flag3 = x2 == 0;
	if (flag3)
	{
		pBestFitness[(x)] = 2E+10f;
		bool flag4 = x == 0;
		if (flag4)
		{
			gBestFitness[(0)] = 2E+10f;
		}
	}
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void rotateVertices( float* positions, int positionsLen0,  CudaPSOCudaVector3* rotatedVertices, int rotatedVerticesLen0, int handStream)
{
	__shared__ float array[80];

	int arrayLen0 = 80;
	__shared__ CudaPSOCudaJoint array2[80];

	int array2Len0 = 80;
	int x = blockIdx.x;
	int x2 = threadIdx.x;
	int num = x * 25;
	int num2 = x * 20;
	bool flag = handStream == 0;
	CudaPSOCudaVertex cudaVertex;
	if (flag)
	{
		cudaVertex = dev_leftHandVertices[(x2)];
	}
	else
	{
		cudaVertex = dev_rightHandVertices[(x2)];
	}
	CudaPSOCudaJoint joint = cudaVertex.joint0;
	CudaPSOCudaJoint joint2 = cudaVertex.joint1;
	CudaPSOCudaJoint joint3 = cudaVertex.joint2;
	CudaPSOCudaJoint joint4 = cudaVertex.joint3;
	float num3 = positions[(num + joint.dimensionIndex)] * 0.01745329f * (float)(1 - joint.isIdentity);
	float num4 = positions[(num + joint2.dimensionIndex)] * 0.01745329f * (float)(1 - joint2.isIdentity);
	float num5 = positions[(num + joint3.dimensionIndex)] * 0.01745329f * (float)(1 - joint3.isIdentity);
	float num6 = positions[(num + joint4.dimensionIndex)] * 0.01745329f * (float)(1 - joint4.isIdentity);
	CudaPSOCudaVector3 position = cudaVertex.position;
	float value = num3;
	CudaPSOCudaVector3 axis = joint.axis;
	CudaPSOCudaVector3 translation = joint.translation;
	float x3 = axis.x;
	float y = axis.y;
	float z = axis.z;
	float num7 = cosf(value);
	float num8 = sinf(value);
	float num9 = 1.0f - num7;
	float num10 = num9 * x3;
	float num11 = num9 * y;
	float num12 = num9 * z;
	float num13 = num8 * x3;
	float num14 = num8 * y;
	float num15 = num8 * z;
	float num16 = num10 * y;
	float num17 = num10 * z;
	float num18 = num11 * z;
	CudaPSOCudaVector3 r;
	r.x = num10 * x3 + num7;
	r.y = num16 - num15;
	r.z = num17 + num14;
	CudaPSOCudaVector3 r2;
	r2.x = num16 + num15;
	r2.y = num11 * y + num7;
	r2.z = num18 - num13;
	CudaPSOCudaVector3 r3;
	r3.x = num17 - num14;
	r3.y = num18 + num13;
	r3.z = num12 * z + num7;
	position.subtract(translation);
	position.rotate(r, r2, r3);
	position.add(translation);
	value = num4;
	axis = joint2.axis;
	translation = joint2.translation;
	x3 = axis.x;
	y = axis.y;
	z = axis.z;
	num7 = cosf(value);
	num8 = sinf(value);
	num9 = 1.0f - num7;
	num10 = num9 * x3;
	num11 = num9 * y;
	num12 = num9 * z;
	num13 = num8 * x3;
	num14 = num8 * y;
	num15 = num8 * z;
	num16 = num10 * y;
	num17 = num10 * z;
	num18 = num11 * z;
	r.x = num10 * x3 + num7;
	r.y = num16 - num15;
	r.z = num17 + num14;
	r2.x = num16 + num15;
	r2.y = num11 * y + num7;
	r2.z = num18 - num13;
	r3.x = num17 - num14;
	r3.y = num18 + num13;
	r3.z = num12 * z + num7;
	position.subtract(translation);
	position.rotate(r, r2, r3);
	position.add(translation);
	value = num5;
	axis = joint3.axis;
	translation = joint3.translation;
	x3 = axis.x;
	y = axis.y;
	z = axis.z;
	num7 = cosf(value);
	num8 = sinf(value);
	num9 = 1.0f - num7;
	num10 = num9 * x3;
	num11 = num9 * y;
	num12 = num9 * z;
	num13 = num8 * x3;
	num14 = num8 * y;
	num15 = num8 * z;
	num16 = num10 * y;
	num17 = num10 * z;
	num18 = num11 * z;
	r.x = num10 * x3 + num7;
	r.y = num16 - num15;
	r.z = num17 + num14;
	r2.x = num16 + num15;
	r2.y = num11 * y + num7;
	r2.z = num18 - num13;
	r3.x = num17 - num14;
	r3.y = num18 + num13;
	r3.z = num12 * z + num7;
	position.subtract(translation);
	position.rotate(r, r2, r3);
	position.add(translation);
	value = num6;
	axis = joint4.axis;
	translation = joint4.translation;
	x3 = axis.x;
	y = axis.y;
	z = axis.z;
	num7 = cosf(value);
	num8 = sinf(value);
	num9 = 1.0f - num7;
	num10 = num9 * x3;
	num11 = num9 * y;
	num12 = num9 * z;
	num13 = num8 * x3;
	num14 = num8 * y;
	num15 = num8 * z;
	num16 = num10 * y;
	num17 = num10 * z;
	num18 = num11 * z;
	r.x = num10 * x3 + num7;
	r.y = num16 - num15;
	r.z = num17 + num14;
	r2.x = num16 + num15;
	r2.y = num11 * y + num7;
	r2.z = num18 - num13;
	r3.x = num17 - num14;
	r3.y = num18 + num13;
	r3.z = num12 * z + num7;
	position.subtract(translation);
	position.rotate(r, r2, r3);
	position.add(translation);
	float value2 = positions[(num + 3)] * 0.01745329f;
	float value3 = positions[(num + 4)] * 0.01745329f;
	float value4 = positions[(num + 5)] * 0.01745329f;
	float num19 = sinf(value2);
	float num20 = cosf(value2);
	float num21 = sinf(value3);
	float num22 = cosf(value3);
	float num23 = sinf(value4);
	float num24 = cosf(value4);
	r.x = num22 * num24;
	r.y = -num22 * num23;
	r.z = num21;
	r2.x = num20 * num23 + num19 * num21 * num24;
	r2.y = num20 * num24 - num19 * num21 * num23;
	r2.z = -num19 * num22;
	r3.x = num19 * num23 - num20 * num21 * num24;
	r3.y = num19 * num24 + num20 * num21 * num23;
	r3.z = num20 * num22;
	float num25 = positions[(num)];
	float num26 = positions[(num + 1)];
	float num27 = positions[(num + 2)];
	float x4 = r.dot(position) + num25;
	float y2 = r2.dot(position) + num26;
	float z2 = r3.dot(position) + num27;
	position.x = x4;
	position.y = y2;
	position.z = z2;
	rotatedVertices[(num2 + x2)] = position;
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void computePixels( CudaPSOCudaVector3* rotatedVertices, int rotatedVerticesLen0,  CudaPSOCudaVector3* pixels, int pixelsLen0)
{
	int x = threadIdx.x;
	CudaPSOCudaVector3 cudaVector = rotatedVertices[(x)];
	float num = cudaVector.z * 1000.0f;
	float num2 = cudaVector.x / num * 1000.0f;
	float num3 = cudaVector.y / num * 1000.0f;
	float num4 = num2 * num2 + num3 * num3;
	float num5 = num4 * num4;
	float num6 = num5 * num4;
	float num7 = 1.0f + 0.09627487f * num4 + -0.2699704f * num5 + 0.08848914f * num6;
	float num8 = num2 * num7;
	float num9 = num3 * num7;
	float x2 = num8 * 364.5652f + 257.6964f;
	float y = 209.9422f - num9 * 364.5652f;
	CudaPSOCudaVector3 cudaVector2;
	cudaVector2.x = x2;
	cudaVector2.y = y;
	cudaVector2.z = num;
	pixels[(x)] = cudaVector2;
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void computeFitness( float* positions, int positionsLen0,  CudaPSOCudaVector3* rotatedVertices, int rotatedVerticesLen0,  float* pBestFitness, int pBestFitnessLen0,  float* pBestPositions, int pBestPositionsLen0,  unsigned short* depthImage, int depthImageLen0,  int* fingerTips, int fingerTipsLen0)
{
	__shared__ float array[304];

	int arrayLen0 = 304;
	int x = blockIdx.x;
	int num = threadIdx.x >> 4;
	int num2 = threadIdx.x & 15;
	int num3 = num * 16 + num2;
	CudaPSOCudaLine cudaLine = dev_lines[(num)];
	int vertexIndex = cudaLine.vertexIndex0;
	int vertexIndex2 = cudaLine.vertexIndex1;
	CudaPSOCudaVector3 cudaVector = rotatedVertices[(x * 20 + vertexIndex)];
	CudaPSOCudaVector3 cudaVector2 = rotatedVertices[(x * 20 + vertexIndex2)];
	float num4 = cudaVector.x + (cudaVector2.x - cudaVector.x) * (float)(num2 + 1) / 16.0f;
	float num5 = cudaVector.y + (cudaVector2.y - cudaVector.y) * (float)(num2 + 1) / 16.0f;
	float num6 = cudaVector.z + (cudaVector2.z - cudaVector.z) * (float)(num2 + 1) / 16.0f;
	float num7 = num6 * 1000.0f;
	float num8 = num4 / num7 * 1000.0f;
	float num9 = num5 / num7 * 1000.0f;
	float num10 = num8 * num8 + num9 * num9;
	float num11 = num10 * num10;
	float num12 = num11 * num10;
	float num13 = 1.0f + 0.09627487f * num10 + -0.2699704f * num11 + 0.08848914f * num12;
	float num14 = num8 * num13;
	float num15 = num9 * num13;
	float x2 = num14 * 364.5652f + 257.6964f;
	float y = 209.9422f - num15 * 364.5652f;
	CudaPSOCudaVector3 cudaVector3;
	cudaVector3.x = x2;
	cudaVector3.y = y;
	cudaVector3.z = num7;
	int num16 = (int)cudaVector3.x;
	int num17 = (int)cudaVector3.y;
	unsigned short num18 = depthImage[(num17 * 512 + num16)];
	float z = cudaVector3.z;
	float num19 = z - (float)num18;
	float num20 = num19 * num19;
	array[(num3)] = 0.0f;
	bool flag = num == 2 || num == 6 || num == 10 || num == 14 || num == 18;
	if (flag)
	{
		bool flag2 = num2 == 15;
		if (flag2)
		{
			int num21 = 0;
			bool flag3 = num != 2;
			if (flag3)
			{
				num21 = (num - 6 >> 2) + 1;
			}
			int num22 = fingerTips[(num21 * 2)];
			int num23 = fingerTips[(num21 * 2 + 1)];
			float num24 = (float)(num22 - num16);
			num24 *= num24;
			float num25 = (float)(num23 - num17);
			num25 *= num25;
			float num26 = num24 + num25;
			array[(num3)] = num26 * num26;
		}
	}
	__syncthreads();
	int i = 256;
	bool flag4 = num3 >= i;
	if (flag4)
	{
		array[(num3 - i)] += array[(num3)];
	}
	__syncthreads();
	for (i /= 2; i > 0; i /= 2)
	{
		bool flag5 = num3 < i;
		if (flag5)
		{
			array[(num3)] += array[(num3 + i)];
		}
		__syncthreads();
	}
	bool flag6 = num3 == 0;
	if (flag6)
	{
		bool flag7 = array[(0)] < pBestFitness[(x)];
		if (flag7)
		{
			pBestFitness[(x)] = array[(0)];
			for (int j = 0; j < 25; j++)
			{
				pBestPositions[(x * 25 + j)] = positions[(x * 25 + j)];
			}
		}
	}
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void computeFitness2( float* positions, int positionsLen0,  float* pBestFitness, int pBestFitnessLen0,  float* pBestPositions, int pBestPositionsLen0)
{
	int x = blockIdx.x;
	int num = 25 * x;
	float num2 = 0.0f;
	for (int i = 0; i < 25; i++)
	{
		float num3 = positions[(num + i)];
		num2 += num3 * num3;
	}
	bool flag = num2 < pBestFitness[(x)];
	if (flag)
	{
		pBestFitness[(x)] = num2;
		for (int j = 0; j < 25; j++)
		{
			pBestPositions[(num + j)] = positions[(num + j)];
		}
	}
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void findLocalBest( float* pBestFitness, int pBestFitnessLen0,  int* pBestIndex, int pBestIndexLen0)
{
	__shared__ float array[512];

	int arrayLen0 = 512;
	__shared__ int array2[512];

	int array2Len0 = 512;
	int x = threadIdx.x;
	array[(x)] = pBestFitness[(x)];
	array2[(x)] = x;
	__syncthreads();
	for (int i = 256; i > 0; i /= 2)
	{
		bool flag = x < i;
		if (flag)
		{
			int num = array2[(x)];
			int num2 = array2[(x + i)];
			bool flag2 = array[(num)] < array[(num2)];
			if (flag2)
			{
				array2[(x)] = num;
			}
			else
			{
				array2[(x)] = num2;
			}
		}
		__syncthreads();
	}
	bool flag3 = x == 0;
	if (flag3)
	{
		pBestIndex[(0)] = array2[(0)];
	}
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void updateGlobalBest( float* pBestPositions, int pBestPositionsLen0,  float* gBestPositions, int gBestPositionsLen0,  float* pBestFitness, int pBestFitnessLen0,  float* gBestFitness, int gBestFitnessLen0,  int* pBestIndex, int pBestIndexLen0)
{
	int x = threadIdx.x;
	int num = pBestIndex[(0)];
	float num2 = pBestFitness[(num)];
	float num3 = gBestFitness[(0)];
	bool flag = num2 < num3;
	if (flag)
	{
		bool flag2 = x == 0;
		if (flag2)
		{
			gBestFitness[(0)] = num2;
		}
		gBestPositions[(x)] = pBestPositions[(num * 25 + x)];
	}
}
// VirtualKeyboard.CudaPSO
extern "C" __global__  void updateParticlePosition( float* positions, int positionsLen0,  float* velocities, int velocitiesLen0,  float* pBestPositions, int pBestPositionsLen0,  float* gBestPositions, int gBestPositionsLen0,  curandStateXORWOW* state, int stateLen0, int handStream)
{
	int x = threadIdx.x;
	int x2 = blockIdx.x;
	int num = x2 * 25 + x;
	curandStateXORWOW randStateXORWOW = state[(num)];
	float num2 = curand_uniform(&randStateXORWOW);
	float num3 = curand_uniform(&randStateXORWOW);
	state[(num)] = randStateXORWOW;
	float num4 = positions[(num)];
	float num5 = velocities[(num)];
	float num6 = pBestPositions[(num)];
	float num7 = gBestPositions[(x)];
	float num8 = 0.5f;
	float num9 = 1.5f;
	float num10 = 1.5f;
	float num11 = num8 * num5 + num9 * num2 * (num6 - num4) + num10 * num3 * (num7 - num4);
	float num12 = num4 + num11;
	velocities[(num)] = num11;
	bool flag = handStream == 0;
	float num13;
	float num14;
	if (flag)
	{
		num13 = dev_leftLowerLimits[(x)];
		num14 = dev_leftUpperLimits[(x)];
	}
	else
	{
		num13 = dev_rightLowerLimits[(x)];
		num14 = dev_rightUpperLimits[(x)];
	}
	bool flag2 = num12 > num14;
	if (flag2)
	{
		num12 = num14;
	}
	else
	{
		bool flag3 = num12 < num13;
		if (flag3)
		{
			num12 = num13;
		}
	}
	positions[(num)] = num12;
}
// VirtualKeyboard.CudaPSO
__device__  float f1(float x)
{
	return x * x;
}
