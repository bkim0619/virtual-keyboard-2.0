﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cudafy;
using Cudafy.Host;
using Cudafy.Translator;
using Cudafy.Types;
using Cudafy.Rand;
using System.Diagnostics;

namespace VirtualKeyboard
{
    enum HandStream
    {
        Left = 0, Right = 1
    };

    enum HandJoint
    {
        Identity = 0, Thumb0Main, Thumb0Side, Thumb1, Index0Main, Index0Side, Index1, Index2, Middle0Main, Middle0Side, Middle1, Middle2, Ring0Main, Ring0Side, Ring1, Ring2, Pinky0Main, Pinky0Side, Pinky1, Pinky2
    };

    enum HandVertex
    {
        Wrist = 0, Thumb0, Thumb1, Thumb2, Index0, Index1, Index2, Index3, Middle0, Middle1, Middle2, Middle3, Ring0, Ring1, Ring2, Ring3, Pinky0, Pinky1, Pinky2, Pinky3
    };

    class CudaPSO
    {
        public const int depthImageWidth = 512;
        public const int depthImageHeight = 424;
        public const int numOfPixels = depthImageWidth * depthImageHeight;

        public const int numOfParticles = 512;
        public const int numOfIterations = 32;

        public const int numOfDimensions = 25;
        public const int numOfFixedDimensions = 3;
        
        public const int numOfVertices = 20;

        public const int numOfLines = 19;
        public const int numOfSampling = 16; // NOTE: always change together (up) 16
        public const int rightShift = 4; // NOTE: always change together (down) 4
        public const int numOfMaxParallelReduction = 512; // 512

        public const int numOfFingers = 5;
        public const int numOfJoints = 20; // including identity matrix
        public const int numOfRotations = 4;

        public const float conversionFactor = (float)(2 * Math.PI / 360);
        public const float fx = 364.5652f; // focal length x
        public const float fy = 364.5652f; // focal length y
        public const float cx = 257.6964f; // principal point x
        public const float cy = 209.9422f; // principal point y
        public const float k2 = 0.09627487f; // radial distortion second order
        public const float k4 = -0.2699704f; // radial distortion fourth order
        public const float k6 = 0.08848914f; // radial distortion sixth order
        public const float INF = 2e10f;

        private static CudafyModule km;
        private static GPGPU gpu;

        // memory allocation in host
        // constant
        private static float[] rightUpperLimits = new float[numOfDimensions] { 0.3f, 0.3f, 0.9f, 0, 30f, 15f, 30f, 10f, 30f, 30f, 10f, 30f, 30f, 30f, 10f, 30f, 30f, 30f, 10f, 30f, 30f, 30f, 10f, 30f, 30f };
        private static float[] rightLowerLimits = new float[numOfDimensions] { -0.3f, -0.3f, 0.6f, -30f, 0, -15f, 0, -10f, 0, 0, -10f, 0, 0, 0, -10f, 0, 0, 0, -10f, 0, 0, 0, -10f, 0, 0 };

        private static float[] leftUpperLimits = new float[numOfDimensions] { 0.3f, 0.3f, 0.9f, 0, 0, 15f, 30f, 10f, 30f, 30f, 10f, 30f, 30f, 30f, 10f, 30f, 30f, 30f, 10f, 30f, 30f, 30f, 10f, 30f, 30f };
        private static float[] leftLowerLimits = new float[numOfDimensions] { -0.3f, -0.3f, 0.6f, -30f, -30f, -15f, 0, -10f, 0, 0, -10f, 0, 0, 0, -10f, 0, 0, 0, -10f, 0, 0, 0, -10f, 0, 0 };

        private static float[] rightPositions = new float[numOfParticles * numOfDimensions];
        private static float[] leftPositions = new float[numOfParticles * numOfDimensions];

        private static float[] rightVelocities = new float[numOfParticles * numOfDimensions];
        private static float[] leftVelocities = new float[numOfParticles * numOfDimensions];

        private static float[] rightPBestPositions = new float[numOfParticles * numOfDimensions];
        private static float[] leftPBestPositions = new float[numOfParticles * numOfDimensions];

        private static float[] bestPositions = new float[numOfDimensions * 2];
        private static CudaVector3[] bestPixels = new CudaVector3[numOfVertices * 2];

        private static IntPtr rightGBestPositions;
        private static IntPtr leftGBestPositions;

        private static float[] rightPBestFitness = new float[numOfParticles];
        private static float[] leftPBestFitness = new float[numOfParticles];

        private static float[] rightGBestFitness = new float[1];
        private static float[] leftGBestFitness = new float[1];

        private static int[] rightPBestIndex = new int[1];
        private static int[] leftPBestIndex = new int[1];

        private static CudaVector3[] rightRotatedVertices = new CudaVector3[numOfParticles * numOfVertices];
        private static CudaVector3[] leftRotatedVertices = new CudaVector3[numOfParticles * numOfVertices];

        private static float[] rightFixedPositions = new float[numOfFixedDimensions];
        private static float[] leftFixedPositions = new float[numOfFixedDimensions];

        private static float[] globalBestTrend = new float[numOfIterations];


        private static IntPtr leftPixels;
        private static IntPtr rightPixels;

        // memory allocation in GPU
        private static ushort[] dev_leftHandDepths;
        private static ushort[] dev_rightHandDepths;

        private static float[] dev_rightPositions;
        private static float[] dev_leftPositions;

        private static float[] dev_rightVelocities;
        private static float[] dev_leftVelocities;

        private static float[] dev_rightPBestPositions;
        private static float[] dev_leftPBestPositions;

        private static float[] dev_rightGBestPositions;
        private static float[] dev_leftGBestPositions;

        private static float[] dev_rightPBestFitness;
        private static float[] dev_leftPBestFitness;

        private static float[] dev_rightGBestFitness;
        private static float[] dev_leftGBestFitness;

        private static int[] dev_rightPBestIndex;
        private static int[] dev_leftPBestIndex;

        private static RandStateXORWOW[] dev_rightStates;
        private static RandStateXORWOW[] dev_leftStates;

        private static CudaVector3[] dev_rightRotatedVertices;
        private static CudaVector3[] dev_leftRotatedVertices;

        private static int[] dev_rightFingerTips;
        private static int[] dev_leftFingerTips;

        private static float[] dev_rightFixedPositions;
        private static float[] dev_leftFixedPositions;

        private static CudaVector3[] dev_leftPixels;
        private static CudaVector3[] dev_rightPixels;

        private static float[] dev_globalBestTrend;

        // async pointers
        private static IntPtr leftDepths;
        private static IntPtr rightDepths;
        private static IntPtr leftTips;
        private static IntPtr rightTips;
        private static IntPtr leftFixed;
        private static IntPtr rightFixed;

        public static void setupCuda()
        {
            km = CudafyTranslator.Cudafy();
            gpu = CudafyHost.GetDevice(CudafyModes.Target, CudafyModes.DeviceId);
            gpu.LoadModule(km);

            // constant memory
            // parameter limits
            gpu.CopyToConstantMemory(rightUpperLimits, dev_rightUpperLimits);
            gpu.CopyToConstantMemory(rightLowerLimits, dev_rightLowerLimits);

            gpu.CopyToConstantMemory(leftUpperLimits, dev_leftUpperLimits);
            gpu.CopyToConstantMemory(leftLowerLimits, dev_leftLowerLimits);

            // allocate the memory on the GPU
            dev_leftHandDepths = gpu.Allocate<ushort>(numOfPixels);
            dev_rightHandDepths = gpu.Allocate<ushort>(numOfPixels);

            dev_rightPositions = gpu.Allocate<float>(rightPositions);
            dev_leftPositions = gpu.Allocate<float>(leftPositions);

            dev_rightVelocities = gpu.Allocate<float>(rightVelocities);
            dev_leftVelocities = gpu.Allocate<float>(leftVelocities);

            dev_rightPBestPositions = gpu.Allocate<float>(rightPBestPositions);
            dev_leftPBestPositions = gpu.Allocate<float>(leftPBestPositions);

            dev_rightGBestPositions = gpu.Allocate<float>(numOfDimensions);
            dev_leftGBestPositions = gpu.Allocate<float>(numOfDimensions);

            dev_rightPBestFitness = gpu.Allocate<float>(rightPBestFitness);
            dev_leftPBestFitness = gpu.Allocate<float>(leftPBestFitness);

            dev_rightGBestFitness = gpu.Allocate<float>(rightGBestFitness);
            dev_leftGBestFitness = gpu.Allocate<float>(leftGBestFitness);

            dev_rightPBestIndex = gpu.Allocate<int>(rightPBestIndex);
            dev_leftPBestIndex = gpu.Allocate<int>(leftPBestIndex);

            dev_rightStates = gpu.Allocate<RandStateXORWOW>(numOfParticles * numOfDimensions);
            dev_leftStates = gpu.Allocate<RandStateXORWOW>(numOfParticles * numOfDimensions);

            dev_rightRotatedVertices = gpu.Allocate<CudaVector3>(numOfParticles * numOfVertices);
            dev_leftRotatedVertices = gpu.Allocate<CudaVector3>(numOfParticles * numOfVertices);

            dev_rightFingerTips = gpu.Allocate<int>(numOfFingers * 2);
            dev_leftFingerTips = gpu.Allocate<int>(numOfFingers * 2);

            dev_rightFixedPositions = gpu.Allocate<float>(numOfFixedDimensions);
            dev_leftFixedPositions = gpu.Allocate<float>(numOfFixedDimensions);

            dev_leftPixels = gpu.Allocate<CudaVector3>(numOfVertices);
            dev_rightPixels = gpu.Allocate<CudaVector3>(numOfVertices);

            dev_globalBestTrend = gpu.Allocate<float>(globalBestTrend);

            // vertices and joints
            float[,] vertexInformation = new float[numOfVertices, 2] {{0, 0 },
                                                                      { -0.05f, 0.05f }, {0,0 }, {0,0 },
                                                                      { -0.025f, 0.085f }, {0,0 }, {0,0 }, {0,0 },
                                                                      { 0, 0.09f }, {0,0 }, {0,0 }, {0,0 },
                                                                      { 0.02f, 0.085f }, {0,0 }, {0,0 }, {0,0 },
                                                                      { 0.035f, 0.07f }, {0,0 }, {0,0 }, {0,0 }, };
            float[][] fingerLengths = new float[numOfFingers][];
            fingerLengths[0] = new float[3] { 70f, 38f, 28f };
            fingerLengths[1] = new float[4] { 90f, 43f, 24f, 20f };
            fingerLengths[2] = new float[4] { 90f, 43f, 30f, 22f };
            fingerLengths[3] = new float[4] { 90f, 35f, 28f, 25f };
            fingerLengths[4] = new float[4] { 80f, 28f, 20f, 20f };

            for (int i = 0; i < numOfFingers; i++)
            {
                int baseIndex = (i == 0) ? 1 : 4 + 4 * (i - 1);
                float vectorX = vertexInformation[baseIndex, 0];
                float vectorY = vertexInformation[baseIndex, 1];

                for (int j = 0; j < fingerLengths[i].Length - 1; j++)
                {
                    float ratio = fingerLengths[i][j + 1] / fingerLengths[i][j];
                    vectorX *= ratio;
                    vectorY *= ratio;

                    float newX = vertexInformation[baseIndex, 0] + vectorX;
                    float newY = vertexInformation[baseIndex, 1] + vectorY;

                    vertexInformation[baseIndex + 1, 0] = newX;
                    vertexInformation[baseIndex + 1, 1] = newY;

                    baseIndex += 1;
                }
            }
            CudaVector3[] rightHandVertices = new CudaVector3[numOfVertices];
            for (int i = 0; i < rightHandVertices.Length; i++)
            {
                rightHandVertices[i].x = vertexInformation[i, 0];
                rightHandVertices[i].y = vertexInformation[i, 1];
                rightHandVertices[i].z = 0;
            }

            CudaJoint[] cudaJoints = getCudaJoints(rightHandVertices);
            CudaVertex[] rightCudaVertices = getCudaVertices(rightHandVertices, cudaJoints);

            gpu.CopyToConstantMemory(rightCudaVertices, dev_rightHandVertices);

            // left hand
            CudaVector3[] leftHandVertices = mirrorVertices(rightHandVertices);
            cudaJoints = getCudaJoints(leftHandVertices);
            CudaVertex[] leftCudaVertices = getCudaVertices(leftHandVertices, cudaJoints);

            gpu.CopyToConstantMemory(leftCudaVertices, dev_leftHandVertices);

            // lines for computing fitness
            CudaLine[] cudaLines = getCudaLines();
            gpu.CopyToConstantMemory(cudaLines, dev_lines);

            // set up random number generator
            ulong seed = (ulong)DateTime.Now.ToFileTime();
            gpu.Launch(numOfParticles, numOfDimensions).setupRandom(dev_rightStates, seed);

            seed = (ulong)DateTime.Now.ToFileTime();
            gpu.Launch(numOfParticles, numOfDimensions).setupRandom(dev_leftStates, seed);

            // IntPtrs needed for async
            leftDepths = gpu.HostAllocate<ushort>(numOfPixels);
            rightDepths = gpu.HostAllocate<ushort>(numOfPixels);
            leftTips = gpu.HostAllocate<int>(numOfFingers * 2);
            rightTips = gpu.HostAllocate<int>(numOfFingers * 2);
            leftFixed = gpu.HostAllocate<float>(numOfFixedDimensions);
            rightFixed = gpu.HostAllocate<float>(numOfFixedDimensions);

            rightGBestPositions = gpu.HostAllocate<float>(numOfDimensions);
            leftGBestPositions = gpu.HostAllocate<float>(numOfDimensions);

            leftPixels = gpu.HostAllocate<CudaVector3>(numOfVertices);
            rightPixels = gpu.HostAllocate<CudaVector3>(numOfVertices);
        }

        private static CudaVector3[] mirrorVertices(CudaVector3[] vertices)
        {
            CudaVector3[] toReturn = new CudaVector3[vertices.Length];

            for (int i = 0; i < vertices.Length; i++)
            {
                CudaVector3 vertex = vertices[i];
                CudaVector3 mirrored = new CudaVector3();
                mirrored.x = -vertex.x;
                mirrored.y = vertex.y;
                mirrored.z = vertex.z;

                toReturn[i] = mirrored;
            }

            return toReturn;
        }

        private static CudaLine getCudaLine(HandVertex baseEnum, HandVertex targetEnum)
        {
            CudaLine line = new CudaLine();
            line.vertexIndex0 = (int)baseEnum;
            line.vertexIndex1 = (int)targetEnum;
            return line;
        }

        private static CudaLine[] getCudaLines()
        {
            CudaLine[] toReturn = new CudaLine[numOfLines];

            toReturn[0] = getCudaLine(HandVertex.Wrist, HandVertex.Thumb0);
            toReturn[1] = getCudaLine(HandVertex.Thumb0, HandVertex.Thumb1);
            toReturn[2] = getCudaLine(HandVertex.Thumb1, HandVertex.Thumb2);
            toReturn[3] = getCudaLine(HandVertex.Wrist, HandVertex.Index0);
            toReturn[4] = getCudaLine(HandVertex.Index0, HandVertex.Index1);
            toReturn[5] = getCudaLine(HandVertex.Index1, HandVertex.Index2);
            toReturn[6] = getCudaLine(HandVertex.Index2, HandVertex.Index3);
            toReturn[7] = getCudaLine(HandVertex.Wrist, HandVertex.Middle0);
            toReturn[8] = getCudaLine(HandVertex.Middle0, HandVertex.Middle1);
            toReturn[9] = getCudaLine(HandVertex.Middle1, HandVertex.Middle2);
            toReturn[10] = getCudaLine(HandVertex.Middle2, HandVertex.Middle3);
            toReturn[11] = getCudaLine(HandVertex.Wrist, HandVertex.Ring0);
            toReturn[12] = getCudaLine(HandVertex.Ring0, HandVertex.Ring1);
            toReturn[13] = getCudaLine(HandVertex.Ring1, HandVertex.Ring2);
            toReturn[14] = getCudaLine(HandVertex.Ring2, HandVertex.Ring3);
            toReturn[15] = getCudaLine(HandVertex.Wrist, HandVertex.Pinky0);
            toReturn[16] = getCudaLine(HandVertex.Pinky0, HandVertex.Pinky1);
            toReturn[17] = getCudaLine(HandVertex.Pinky1, HandVertex.Pinky2);
            toReturn[18] = getCudaLine(HandVertex.Pinky2, HandVertex.Pinky3);

            return toReturn;
        }

        private static CudaJoint getCudaJoint(HandVertex baseEnum, HandVertex targetEnum, int dimensionIndex, CudaVector3[] vertices)
        {
            int baseIndex = (int)baseEnum;
            int targetIndex = (int)targetEnum;

            CudaJoint joint = new CudaJoint();

            CudaVector3 baseVertex = vertices[baseIndex];
            CudaVector3 targetVertex = vertices[targetIndex];

            joint.axis = getRotationAxis(baseVertex, targetVertex);
            joint.translation = baseVertex;

            joint.dimensionIndex = dimensionIndex;
            joint.isIdentity = 0;

            return joint;
        }

        private static CudaJoint getCudaJoint(HandVertex baseEnum, int dimensionIndex, CudaVector3[] vertices)
        {
            int baseIndex = (int)baseEnum;

            CudaJoint joint = new CudaJoint();

            CudaVector3 rotationAxis = new CudaVector3();
            rotationAxis.x = 0;
            rotationAxis.y = 0;
            rotationAxis.z = 1;

            CudaVector3 translation = vertices[baseIndex];

            joint.axis = rotationAxis;
            joint.translation = translation;
            joint.dimensionIndex = dimensionIndex;
            joint.isIdentity = 0;

            return joint;
        }

        private static CudaJoint getCudaJoint()
        {
            CudaJoint joint = new CudaJoint();

            CudaVector3 rotationAxis = new CudaVector3();
            rotationAxis.x = 1;
            rotationAxis.y = 0;
            rotationAxis.z = 0;

            CudaVector3 translation = new CudaVector3();
            translation.x = 0;
            translation.y = 0;
            translation.z = 0;

            joint.axis = rotationAxis;
            joint.translation = translation;
            joint.isIdentity = 1;
            joint.dimensionIndex = 0;

            return joint;
        }

        private static CudaJoint[] getCudaJoints(CudaVector3[] vertices)
        {
            Dictionary<HandJoint, CudaJoint> dictionary = new Dictionary<HandJoint, CudaJoint>();

            dictionary[HandJoint.Identity] = getCudaJoint();

            dictionary[HandJoint.Thumb0Main] = getCudaJoint(HandVertex.Thumb0, HandVertex.Thumb1, 6, vertices);
            dictionary[HandJoint.Thumb0Side] = getCudaJoint(HandVertex.Thumb0, 7, vertices);
            dictionary[HandJoint.Thumb1] = getCudaJoint(HandVertex.Thumb1, HandVertex.Thumb2, 8, vertices);

            dictionary[HandJoint.Index0Main] = getCudaJoint(HandVertex.Index0, HandVertex.Index1, 9, vertices);
            dictionary[HandJoint.Index0Side] = getCudaJoint(HandVertex.Index0, 10, vertices);
            dictionary[HandJoint.Index1] = getCudaJoint(HandVertex.Index1, HandVertex.Index2, 11, vertices);
            dictionary[HandJoint.Index2] = getCudaJoint(HandVertex.Index2, HandVertex.Index3, 12, vertices);

            dictionary[HandJoint.Middle0Main] = getCudaJoint(HandVertex.Middle0, HandVertex.Middle1, 13, vertices);
            dictionary[HandJoint.Middle0Side] = getCudaJoint(HandVertex.Middle0, 14, vertices);
            dictionary[HandJoint.Middle1] = getCudaJoint(HandVertex.Middle1, HandVertex.Middle2, 15, vertices);
            dictionary[HandJoint.Middle2] = getCudaJoint(HandVertex.Middle2, HandVertex.Middle3, 16, vertices);

            dictionary[HandJoint.Ring0Main] = getCudaJoint(HandVertex.Ring0, HandVertex.Ring1, 17, vertices);
            dictionary[HandJoint.Ring0Side] = getCudaJoint(HandVertex.Ring0, 18, vertices);
            dictionary[HandJoint.Ring1] = getCudaJoint(HandVertex.Ring1, HandVertex.Ring2, 19, vertices);
            dictionary[HandJoint.Ring2] = getCudaJoint(HandVertex.Ring2, HandVertex.Ring3, 20, vertices);

            dictionary[HandJoint.Pinky0Main] = getCudaJoint(HandVertex.Pinky0, HandVertex.Pinky1, 21, vertices);
            dictionary[HandJoint.Pinky0Side] = getCudaJoint(HandVertex.Pinky0, 22, vertices);
            dictionary[HandJoint.Pinky1] = getCudaJoint(HandVertex.Pinky1, HandVertex.Pinky2, 23, vertices);
            dictionary[HandJoint.Pinky2] = getCudaJoint(HandVertex.Pinky2, HandVertex.Pinky3, 24, vertices);

            CudaJoint[] joints = new CudaJoint[numOfJoints];
            foreach (HandJoint joint in Enum.GetValues(typeof(HandJoint)))
            {
                joints[(int)joint] = dictionary[joint];
            }

            return joints;
        }

        private static CudaVertex getCudaVertex(HandVertex vertexEnum, HandJoint joint0, HandJoint joint1, HandJoint joint2, HandJoint joint3, CudaVector3[] vertices, CudaJoint[] joints)
        {
            CudaVertex vertex = new CudaVertex();
            vertex.position = vertices[(int)vertexEnum];
            vertex.joint0 = joints[(int)joint0];
            vertex.joint1 = joints[(int)joint1];
            vertex.joint2 = joints[(int)joint2];
            vertex.joint3 = joints[(int)joint3];

            return vertex;
        }

        private static CudaVertex getCudaVertex(HandVertex vertexEnum, CudaVector3[] vertices, CudaJoint[] joints)
        {
            CudaVertex vertex = new CudaVertex();
            vertex.position = vertices[(int)vertexEnum];
            vertex.joint0 = joints[(int)HandJoint.Identity];
            vertex.joint1 = joints[(int)HandJoint.Identity];
            vertex.joint2 = joints[(int)HandJoint.Identity];
            vertex.joint3 = joints[(int)HandJoint.Identity];
            return vertex;
        }

        private static CudaVertex[] getCudaVertices(CudaVector3[] vertices, CudaJoint[] joints)
        {
            Dictionary<HandVertex, CudaVertex> dictionary = new Dictionary<HandVertex, CudaVertex>();

            dictionary[HandVertex.Wrist] = getCudaVertex(HandVertex.Wrist, vertices, joints);
            dictionary[HandVertex.Thumb0] = getCudaVertex(HandVertex.Thumb0, vertices, joints);
            dictionary[HandVertex.Thumb1] = getCudaVertex(HandVertex.Thumb1, HandJoint.Identity, HandJoint.Identity, HandJoint.Thumb0Side, HandJoint.Thumb0Main, vertices, joints);
            dictionary[HandVertex.Thumb2] = getCudaVertex(HandVertex.Thumb2, HandJoint.Identity, HandJoint.Thumb1, HandJoint.Thumb0Side, HandJoint.Thumb0Main, vertices, joints);
            dictionary[HandVertex.Index0] = getCudaVertex(HandVertex.Index0, vertices, joints);
            dictionary[HandVertex.Index1] = getCudaVertex(HandVertex.Index1, HandJoint.Identity, HandJoint.Identity, HandJoint.Index0Side, HandJoint.Index0Main, vertices, joints);
            dictionary[HandVertex.Index2] = getCudaVertex(HandVertex.Index2, HandJoint.Identity, HandJoint.Index1, HandJoint.Index0Side, HandJoint.Index0Main, vertices, joints);
            dictionary[HandVertex.Index3] = getCudaVertex(HandVertex.Index3, HandJoint.Index2, HandJoint.Index1, HandJoint.Index0Side, HandJoint.Index0Main, vertices, joints);
            dictionary[HandVertex.Middle0] = getCudaVertex(HandVertex.Middle0, vertices, joints);
            dictionary[HandVertex.Middle1] = getCudaVertex(HandVertex.Middle1, HandJoint.Identity, HandJoint.Identity, HandJoint.Middle0Side, HandJoint.Middle0Main, vertices, joints);
            dictionary[HandVertex.Middle2] = getCudaVertex(HandVertex.Middle2, HandJoint.Identity, HandJoint.Middle1, HandJoint.Middle0Side, HandJoint.Middle0Main, vertices, joints);
            dictionary[HandVertex.Middle3] = getCudaVertex(HandVertex.Middle3, HandJoint.Middle2, HandJoint.Middle1, HandJoint.Middle0Side, HandJoint.Middle0Main, vertices, joints);
            dictionary[HandVertex.Ring0] = getCudaVertex(HandVertex.Ring0, vertices, joints);
            dictionary[HandVertex.Ring1] = getCudaVertex(HandVertex.Ring1, HandJoint.Identity, HandJoint.Identity, HandJoint.Ring0Side, HandJoint.Ring0Main, vertices, joints);
            dictionary[HandVertex.Ring2] = getCudaVertex(HandVertex.Ring2, HandJoint.Identity, HandJoint.Ring1, HandJoint.Ring0Side, HandJoint.Ring0Main, vertices, joints);
            dictionary[HandVertex.Ring3] = getCudaVertex(HandVertex.Ring3, HandJoint.Ring2, HandJoint.Ring1, HandJoint.Ring0Side, HandJoint.Ring0Main, vertices, joints);
            dictionary[HandVertex.Pinky0] = getCudaVertex(HandVertex.Pinky0, vertices, joints);
            dictionary[HandVertex.Pinky1] = getCudaVertex(HandVertex.Pinky1, HandJoint.Identity, HandJoint.Identity, HandJoint.Pinky0Side, HandJoint.Pinky0Main, vertices, joints);
            dictionary[HandVertex.Pinky2] = getCudaVertex(HandVertex.Pinky2, HandJoint.Identity, HandJoint.Pinky1, HandJoint.Pinky0Side, HandJoint.Pinky0Main, vertices, joints);
            dictionary[HandVertex.Pinky3] = getCudaVertex(HandVertex.Pinky3, HandJoint.Pinky2, HandJoint.Pinky1, HandJoint.Pinky0Side, HandJoint.Pinky0Main, vertices, joints);

            CudaVertex[] toReturn = new CudaVertex[numOfVertices];
            foreach (HandVertex vertex in Enum.GetValues(typeof(HandVertex)))
            {
                toReturn[(int)vertex] = dictionary[vertex];
            }

            return toReturn;
        }

        private static CudaVector3 getRotationAxis(CudaVector3 baseVertex, CudaVector3 targetVertex)
        {
            CudaVector3 vector = new CudaVector3();
            vector.x = targetVertex.x - baseVertex.x;
            vector.y = targetVertex.y - baseVertex.y;
            vector.z = targetVertex.z - baseVertex.z;

            // rotate counter-clockwise 90 degrees
            float tmp = vector.x;
            vector.x = -vector.y;
            vector.y = tmp;

            // normalize
            float length = (float)(Math.Sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z));
            vector.x /= length;
            vector.y /= length;
            vector.z /= length;

            return vector;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="leftHandDepths">Left hand depth map</param>
        /// <param name="rightHandDepths">Right hand depth map</param>
        /// <param name="leftFingerTips">Pixel coordinates of left hand fingertips</param>
        /// <param name="rightFingerTips">Pixel coordinates of right hand fingertips</param>
        /// <param name="leftFixedPositions">Global coordinates of left wrist</param>
        /// <param name="rightFixedPositions">Global coordinates of right wrist</param>
        /// <returns>Float array of best pose for both hands, length=numOfDimensions*2</returns>
        public static float run(ushort[] leftHandDepths, ushort[] rightHandDepths, int[] leftFingerTips, int[] rightFingerTips, float[] leftFixedPositions, float[] rightFixedPositions)
        {
            Stopwatch sw = new Stopwatch();
            gpu.StartTimer();
            sw.Start();

            for (int i = 0; i < leftHandDepths.Length; i++)
            {
                leftDepths.Set(i, leftHandDepths[i]);
                rightDepths.Set(i, rightHandDepths[i]);
            }

            for (int i = 0; i < leftFingerTips.Length; i++)
            {
                leftTips.Set(i, leftFingerTips[i]);
                rightTips.Set(i, rightFingerTips[i]);
            }

            for (int i = 0; i < leftFixedPositions.Length; i++)
            {
                leftFixed.Set(i, leftFixedPositions[i]);
                rightFixed.Set(i, rightFixedPositions[i]);
            }


            // copy arrays into GPU
            gpu.CopyToDeviceAsync(leftDepths, 0, dev_leftHandDepths, 0, leftHandDepths.Length, (int)HandStream.Left);
            gpu.CopyToDeviceAsync(leftTips, 0, dev_leftFingerTips, 0, leftFingerTips.Length, (int)HandStream.Left);

            gpu.CopyToDeviceAsync(rightDepths, 0, dev_rightHandDepths, 0, rightHandDepths.Length, (int)HandStream.Right);
            gpu.CopyToDeviceAsync(rightTips, 0, dev_rightFingerTips, 0, rightFingerTips.Length, (int)HandStream.Right);

            gpu.CopyToDeviceAsync(leftFixed, 0, dev_leftFixedPositions, 0, leftFixedPositions.Length, (int)HandStream.Left);
            gpu.CopyToDeviceAsync(rightFixed, 0, dev_rightFixedPositions, 0, rightFixedPositions.Length, (int)HandStream.Right);

            gpu.LaunchAsync(numOfParticles, numOfDimensions, (int)HandStream.Left, "initialize", dev_leftPositions, dev_leftVelocities, dev_leftPBestFitness, dev_leftGBestFitness, dev_leftStates, dev_leftFixedPositions, (int)HandStream.Left);
            gpu.LaunchAsync(numOfParticles, numOfDimensions, (int)HandStream.Right, "initialize", dev_rightPositions, dev_rightVelocities, dev_rightPBestFitness, dev_rightGBestFitness, dev_rightStates, dev_rightFixedPositions, (int)HandStream.Right);
            //gpu.Launch(numOfParticles, numOfDimensions).initialize(dev_rightPositions, dev_rightVelocities, dev_rightPBestFitness, dev_rightGBestFitness, dev_rightStates, dev_rightFixedPositions);

            for (int i = 0; i < numOfIterations; i++)
            {
                gpu.LaunchAsync(numOfParticles, numOfVertices, (int)HandStream.Left, "rotateVertices", dev_leftPositions, dev_leftRotatedVertices, (int)HandStream.Left);
                gpu.LaunchAsync(numOfParticles, numOfVertices, (int)HandStream.Right, "rotateVertices", dev_rightPositions, dev_rightRotatedVertices, (int)HandStream.Right);
                //gpu.Launch(numOfParticles, numOfVertices).rotateVertices(dev_rightPositions, dev_rightRotatedVertices);

                gpu.LaunchAsync(numOfParticles, numOfLines * numOfSampling, (int)HandStream.Left, "computeFitness", dev_leftPositions, dev_leftRotatedVertices, dev_leftPBestFitness, dev_leftPBestPositions, dev_leftHandDepths, dev_leftFingerTips);
                gpu.LaunchAsync(numOfParticles, numOfLines * numOfSampling, (int)HandStream.Right, "computeFitness", dev_rightPositions, dev_rightRotatedVertices, dev_rightPBestFitness, dev_rightPBestPositions, dev_rightHandDepths, dev_rightFingerTips);
                //gpu.Launch(numOfParticles, numOfLines * numOfSampling).computeFitness(dev_rightPositions, dev_rightRotatedVertices, dev_rightPBestFitness, dev_rightPBestPositions, dev_leftHandDepths, dev_leftFingerTips);

                gpu.LaunchAsync(1, numOfParticles, (int)HandStream.Left, "findLocalBest", dev_leftPBestFitness, dev_leftPBestIndex);
                gpu.LaunchAsync(1, numOfParticles, (int)HandStream.Right, "findLocalBest", dev_rightPBestFitness, dev_rightPBestIndex);
                //gpu.Launch(1, numOfParticles).findLocalBest(dev_rightPBestFitness, dev_rightPBestIndex);

                gpu.LaunchAsync(1, numOfDimensions, (int)HandStream.Left, "updateGlobalBest", dev_leftPBestPositions, dev_leftGBestPositions, dev_leftPBestFitness, dev_leftGBestFitness, dev_leftPBestIndex);
                gpu.LaunchAsync(1, numOfDimensions, (int)HandStream.Right, "updateGlobalBest", dev_rightPBestPositions, dev_rightGBestPositions, dev_rightPBestFitness, dev_rightGBestFitness, dev_rightPBestIndex);
                //gpu.Launch(1, numOfDimensions).updateGlobalBest(dev_rightPBestPositions, dev_rightGBestPositions, dev_rightPBestFitness, dev_rightGBestFitness, dev_rightPBestIndex);

                gpu.LaunchAsync(numOfParticles, numOfDimensions, (int)HandStream.Left, "updateParticlePosition", dev_leftPositions, dev_leftVelocities, dev_leftPBestPositions, dev_leftGBestPositions, dev_leftStates, (int)HandStream.Left);
                gpu.LaunchAsync(numOfParticles, numOfDimensions, (int)HandStream.Right, "updateParticlePosition", dev_rightPositions, dev_rightVelocities, dev_rightPBestPositions, dev_rightGBestPositions, dev_rightStates, (int)HandStream.Right);
                //gpu.Launch(numOfParticles, numOfDimensions).updateParticlePosition(dev_rightPositions, dev_rightVelocities, dev_rightPBestPositions, dev_rightGBestPositions, dev_rightStates);

                
                gpu.LaunchAsync(1, 1, (int)HandStream.Left, "trackGlobalBest", dev_leftGBestFitness, dev_globalBestTrend, i);
            }

            // rotate and pixelate
            gpu.LaunchAsync(1, numOfVertices, (int)HandStream.Left, "rotateVertices", dev_leftGBestPositions, dev_leftRotatedVertices, (int)HandStream.Left);
            gpu.LaunchAsync(1, numOfVertices, (int)HandStream.Right, "rotateVertices", dev_rightGBestPositions, dev_rightRotatedVertices, (int)HandStream.Right);

            //gpu.LaunchAsync(1, numOfVertices, (int)HandStream.Left, "computePixels", dev_leftRotatedVertices, dev_leftPixels);
            //gpu.LaunchAsync(1, numOfVertices, (int)HandStream.Right, "computePixels", dev_rightRotatedVertices, dev_rightPixels);

            //gpu.CopyFromDeviceAsync(dev_leftGBestPositions, 0, leftGBestPositions, 0, numOfDimensions, (int)HandStream.Left);
            //gpu.CopyFromDeviceAsync(dev_rightGBestPositions, 0, rightGBestPositions, 0, numOfDimensions, (int)HandStream.Right);

            gpu.CopyFromDeviceAsync(dev_leftRotatedVertices, 0, leftPixels, 0, numOfVertices, (int)HandStream.Left);
            gpu.CopyFromDeviceAsync(dev_rightRotatedVertices, 0, rightPixels, 0, numOfVertices, (int)HandStream.Right);
            //gpu.CopyFromDeviceAsync(dev_leftPixels, 0, leftPixels, 0, numOfVertices, (int)HandStream.Left);
            //gpu.CopyFromDeviceAsync(dev_rightPixels, 0, rightPixels, 0, numOfVertices, (int)HandStream.Right);

            gpu.SynchronizeStream((int)HandStream.Left);
            gpu.SynchronizeStream((int)HandStream.Right);

            //printShortArray("global best trend", globalBestTrend);
            gpu.CopyFromDevice(dev_leftGBestFitness, leftGBestFitness);
            gpu.CopyFromDevice(dev_rightGBestFitness, rightGBestFitness);
            //gpu.CopyFromDevice(dev_rightPositions, rightPositions);
            //gpu.CopyFromDevice(dev_rightVelocities, rightVelocities);
            //gpu.CopyFromDevice(dev_rightPBestFitness, rightPBestFitness);
            //gpu.CopyFromDevice(dev_rightPBestPositions, rightPBestPositions);
            //gpu.CopyFromDevice(dev_rightPBestIndex, rightPBestIndex);

            //printLongArray("position", currPositions);
            //printLongArray("velocity", currVelocities);
            //printShortArray("pBest", pBestFitness);
            //printShortArray("gBest", gBestFitness);
            //printShortArray("gBestPosition", gBestPositions);


            //printShortArray("left gBest", leftGBestFitness);
            //printShortArray("right gBest", rightGBestFitness);
            //printShortArray("gBestPosition", rightGBestPositions);

            //GPGPU.CopyOnHost(leftGBestPositions, 0, bestPositions, 0, numOfDimensions);
            //GPGPU.CopyOnHost(rightGBestPositions, 0, bestPositions, numOfDimensions, numOfDimensions);

            GPGPU.CopyOnHost(leftPixels, 0, bestPixels, 0, numOfVertices);
            GPGPU.CopyOnHost(rightPixels, 0, bestPixels, numOfVertices, numOfVertices);

            float gpuElapsed = gpu.StopTimer();
            sw.Stop();

            gpu.CopyFromDevice(dev_globalBestTrend, globalBestTrend);

            //printShortArray("best positions:", bestPositions);
            //Debug.WriteLine("total:{0} according to gpu timer", gpuElapsed);
            //Debug.WriteLine("total:{0} according to cpu timer", sw.Elapsed);

            //return gpuElapsed;
            return rightGBestFitness[0];
        }

        public static void end()
        {
            // free the memory allocated on the GPU
            if (gpu != null)
            {
                gpu.FreeAll();
                gpu.HostFreeAll();
                gpu.DestroyStreams();
            }

            Debug.WriteLine("Releasing gpu resources");
        }

        public static void printLongArray(string header, float[] longArray)
        {
            Debug.WriteLine("");
            Debug.WriteLine(header);
            for (int i = 0; i < longArray.Length; i++)
            {
                if (i % numOfDimensions == 0)
                {
                    Debug.WriteLine("Particle {0}", i / numOfDimensions);
                }
                Debug.WriteLine("{0}", longArray[i]);
            }
        }

        public static void printShortArray(string header, float[] shortArray)
        {
            Debug.WriteLine("");
            Debug.WriteLine(header);
            for (int i = 0; i < shortArray.Length; i++)
            {
                Debug.Write(shortArray[i] + "f,");
            }
            Debug.WriteLine("");
        }

        [Cudafy]
        public static float[] dev_rightLowerLimits = new float[numOfDimensions];

        [Cudafy]
        public static float[] dev_rightUpperLimits = new float[numOfDimensions];

        [Cudafy]
        public static float[] dev_leftLowerLimits = new float[numOfDimensions];

        [Cudafy]
        public static float[] dev_leftUpperLimits = new float[numOfDimensions];

        [Cudafy]
        public static void setupRandom(GThread thread, RandStateXORWOW[] state, ulong seed)
        {
            int index = thread.threadIdx.x + thread.blockIdx.x * numOfDimensions;
            thread.curand_init(seed, (ulong)index, 0, ref state[index]);
        }

        [Cudafy]
        public static void testRandom(GThread thread, float[] currPositions, RandStateXORWOW[] state)
        {
            int id = thread.threadIdx.x + thread.blockIdx.x * numOfDimensions;
            RandStateXORWOW localState = state[id];

            currPositions[id] = thread.curand_uniform(ref localState);

            state[id] = localState;
        }

        [Cudafy]
        public static void trackGlobalBest(GThread thread, float[] gBestFitness, float[] globalBestTrend, int iteration)
        {
            globalBestTrend[iteration] = gBestFitness[0];
        }

        [Cudafy]
        public static void initialize(GThread thread, float[] currPositions, float[] currVelocities, float[] pBestFitness, float[] gBestFitness, RandStateXORWOW[] state, float[] fixedPositions, int handStream)
        {
            int particleIndex = thread.blockIdx.x;
            int dimension = thread.threadIdx.x;

            int index = particleIndex * numOfDimensions + dimension;
            RandStateXORWOW localState = state[index];
            float randomFloat = thread.curand_uniform(ref localState);
            state[index] = localState;

            float lower, upper;
            if (handStream == (int)HandStream.Left)
            {
                lower = dev_leftLowerLimits[dimension];
                upper = dev_leftUpperLimits[dimension];
            }
            else
            {
                lower = dev_rightLowerLimits[dimension];
                upper = dev_rightUpperLimits[dimension];
            }

            if (dimension < numOfFixedDimensions)
            {
                currPositions[index] = fixedPositions[dimension];
            }
            else
            {
                currPositions[index] = lower + (upper - lower) * randomFloat;
            }

            /*
            if (dimension == 0)
            {
                currPositions[index] = -0.112845f; // 0.203320f
            }
            else if (dimension == 1)
            {
                currPositions[index] = -0.055823f; //-0.027285f
            }
            else if (dimension == 2)
            {
                currPositions[index] = 0.754f; // 0.766f
            }
            else
            {
                //currPositions[index] = dev_rightLowerLimits[dimension] + (dev_rightUpperLimits[dimension] - dev_rightLowerLimits[dimension]) * randomFloat;
                currPositions[index] = dev_leftLowerLimits[dimension] + (dev_leftUpperLimits[dimension] - dev_leftLowerLimits[dimension]) * randomFloat;
            }*/

            currVelocities[index] = 0;

            if (dimension == 0)
            {
                pBestFitness[particleIndex] = INF;
                if (particleIndex == 0)
                {
                    gBestFitness[0] = INF;
                }
            }
        }

        [Cudafy]
        public static void rotateVertices(GThread thread, float[] positions, CudaVector3[] rotatedVertices, int handStream)
        {
            float[] angles = thread.AllocateShared<float>("angles", numOfRotations * numOfVertices);
            CudaJoint[] joints = thread.AllocateShared<CudaJoint>("joints", numOfRotations * numOfVertices);

            int particleIndex = thread.blockIdx.x;
            int vertexIndex = thread.threadIdx.x;
            int dimensionOffset = particleIndex * numOfDimensions;
            int vertexOffset = particleIndex * numOfVertices;

            CudaVertex originalVertex;
            if (handStream == (int)HandStream.Left)
            {
                originalVertex = dev_leftHandVertices[vertexIndex];
            }
            else
            {
                originalVertex = dev_rightHandVertices[vertexIndex];
            }

            CudaJoint joint0 = originalVertex.joint0;
            CudaJoint joint1 = originalVertex.joint1;
            CudaJoint joint2 = originalVertex.joint2;
            CudaJoint joint3 = originalVertex.joint3;

            float angle0 = positions[dimensionOffset + joint0.dimensionIndex] * conversionFactor * (1 - joint0.isIdentity);
            float angle1 = positions[dimensionOffset + joint1.dimensionIndex] * conversionFactor * (1 - joint1.isIdentity);
            float angle2 = positions[dimensionOffset + joint2.dimensionIndex] * conversionFactor * (1 - joint2.isIdentity);
            float angle3 = positions[dimensionOffset + joint3.dimensionIndex] * conversionFactor * (1 - joint3.isIdentity);

            CudaVector3 rotatedVertex = originalVertex.position;

            CudaVector3 r0, r1, r2;
            // rotation 0
            float angle = angle0;
            CudaVector3 axis = joint0.axis;
            CudaVector3 translation = joint0.translation;

            float x = axis.x;
            float y = axis.y;
            float z = axis.z;
            float c = GMath.Cos(angle);
            float s = GMath.Sin(angle);
            float t = 1 - c;
            float tx = t * x;
            float ty = t * y;
            float tz = t * z;
            float sx = s * x;
            float sy = s * y;
            float sz = s * z;
            float txy = tx * y;
            float txz = tx * z;
            float tyz = ty * z;

            r0.x = tx * x + c;
            r0.y = txy - sz;
            r0.z = txz + sy;
            r1.x = txy + sz;
            r1.y = ty * y + c;
            r1.z = tyz - sx;
            r2.x = txz - sy;
            r2.y = tyz + sx;
            r2.z = tz * z + c;

            rotatedVertex.subtract(translation);
            rotatedVertex.rotate(r0, r1, r2);
            rotatedVertex.add(translation);

            // rotation 1
            angle = angle1;
            axis = joint1.axis;
            translation = joint1.translation;

            x = axis.x;
            y = axis.y;
            z = axis.z;
            c = GMath.Cos(angle);
            s = GMath.Sin(angle);
            t = 1 - c;
            tx = t * x;
            ty = t * y;
            tz = t * z;
            sx = s * x;
            sy = s * y;
            sz = s * z;
            txy = tx * y;
            txz = tx * z;
            tyz = ty * z;

            r0.x = tx * x + c;
            r0.y = txy - sz;
            r0.z = txz + sy;
            r1.x = txy + sz;
            r1.y = ty * y + c;
            r1.z = tyz - sx;
            r2.x = txz - sy;
            r2.y = tyz + sx;
            r2.z = tz * z + c;

            rotatedVertex.subtract(translation);
            rotatedVertex.rotate(r0, r1, r2);
            rotatedVertex.add(translation);


            // rotation 2
            angle = angle2;
            axis = joint2.axis;
            translation = joint2.translation;

            x = axis.x;
            y = axis.y;
            z = axis.z;
            c = GMath.Cos(angle);
            s = GMath.Sin(angle);
            t = 1 - c;
            tx = t * x;
            ty = t * y;
            tz = t * z;
            sx = s * x;
            sy = s * y;
            sz = s * z;
            txy = tx * y;
            txz = tx * z;
            tyz = ty * z;

            r0.x = tx * x + c;
            r0.y = txy - sz;
            r0.z = txz + sy;
            r1.x = txy + sz;
            r1.y = ty * y + c;
            r1.z = tyz - sx;
            r2.x = txz - sy;
            r2.y = tyz + sx;
            r2.z = tz * z + c;

            rotatedVertex.subtract(translation);
            rotatedVertex.rotate(r0, r1, r2);
            rotatedVertex.add(translation);


            // rotation 3
            angle = angle3;
            axis = joint3.axis;
            translation = joint3.translation;

            x = axis.x;
            y = axis.y;
            z = axis.z;
            c = GMath.Cos(angle);
            s = GMath.Sin(angle);
            t = 1 - c;
            tx = t * x;
            ty = t * y;
            tz = t * z;
            sx = s * x;
            sy = s * y;
            sz = s * z;
            txy = tx * y;
            txz = tx * z;
            tyz = ty * z;

            r0.x = tx * x + c;
            r0.y = txy - sz;
            r0.z = txz + sy;
            r1.x = txy + sz;
            r1.y = ty * y + c;
            r1.z = tyz - sx;
            r2.x = txz - sy;
            r2.y = tyz + sx;
            r2.z = tz * z + c;

            rotatedVertex.subtract(translation);
            rotatedVertex.rotate(r0, r1, r2);
            rotatedVertex.add(translation);


            // rotate in 3 dimension
            float alpha = positions[dimensionOffset + 3] * conversionFactor;
            float beta = positions[dimensionOffset + 4] * conversionFactor;
            float gamma = positions[dimensionOffset + 5] * conversionFactor;

            float sinAlpha = GMath.Sin(alpha);
            float cosAlpha = GMath.Cos(alpha);
            float sinBeta = GMath.Sin(beta);
            float cosBeta = GMath.Cos(beta);
            float sinGamma = GMath.Sin(gamma);
            float cosGamma = GMath.Cos(gamma);

            r0.x = cosBeta * cosGamma;
            r0.y = -cosBeta * sinGamma;
            r0.z = sinBeta;

            r1.x = cosAlpha * sinGamma + sinAlpha * sinBeta * cosGamma;
            r1.y = cosAlpha * cosGamma - sinAlpha * sinBeta * sinGamma;
            r1.z = -sinAlpha * cosBeta;

            r2.x = sinAlpha * sinGamma - cosAlpha * sinBeta * cosGamma;
            r2.y = sinAlpha * cosGamma + cosAlpha * sinBeta * sinGamma;
            r2.z = cosAlpha * cosBeta;

            float xTranslation = positions[dimensionOffset];
            float yTranslation = positions[dimensionOffset + 1];
            float zTranslation = positions[dimensionOffset + 2];

            float newX = r0.dot(rotatedVertex) + xTranslation;
            float newY = r1.dot(rotatedVertex) + yTranslation;
            float newZ = r2.dot(rotatedVertex) + zTranslation;

            rotatedVertex.x = newX;
            rotatedVertex.y = newY;
            rotatedVertex.z = newZ;

            rotatedVertices[vertexOffset + vertexIndex] = rotatedVertex;
        }

        [Cudafy]
        public static void computePixels(GThread thread, CudaVector3[] rotatedVertices, CudaVector3[] pixels)
        {
            int vertexIndex = thread.threadIdx.x;
            CudaVector3 rotatedVertex = rotatedVertices[vertexIndex];

            float depth = rotatedVertex.z * 1000;
            float x = rotatedVertex.x / depth * 1000;
            float y = rotatedVertex.y / depth * 1000;

            float rSquared = x * x + y * y;
            float rFourth = rSquared * rSquared;
            float rSixth = rFourth * rSquared;
            float distortionFactor = (1 + k2 * rSquared + k4 * rFourth + k6 * rSixth);

            float xDistorted = x * distortionFactor;
            float yDistorted = y * distortionFactor;

            float xPixel = xDistorted * fx + cx;
            float yPixel = cy - yDistorted * fy;

            CudaVector3 vertexPixel;
            vertexPixel.x = xPixel;
            vertexPixel.y = yPixel;
            vertexPixel.z = depth;

            pixels[vertexIndex] = vertexPixel;
        }

        [Cudafy]
        public static void computeFitness(GThread thread, float[] positions, CudaVector3[] rotatedVertices, float[] pBestFitness, float[] pBestPositions, ushort[] depthImage, int[] fingerTips)
        {
            float[] cache = thread.AllocateShared<float>("cache", numOfLines * numOfSampling);

            int particleIndex = thread.blockIdx.x;
            int lineIndex = thread.threadIdx.x >> rightShift;
            int samplingIndex = thread.threadIdx.x & (numOfSampling - 1);

            int subVertexIndex = lineIndex * numOfSampling + samplingIndex;

            //int baseIndex = dev_lineToBaseVertexMapping[lineIndex];
            //int targetIndex = dev_lineToTargetVertexMapping[lineIndex];

            CudaLine line = dev_lines[lineIndex];

            int baseIndex = line.vertexIndex0;
            int targetIndex = line.vertexIndex1;

            // project camera space into pixel space            
            CudaVector3 baseVertex = rotatedVertices[particleIndex * numOfVertices + baseIndex];
            CudaVector3 rotatedVertex = rotatedVertices[particleIndex * numOfVertices + targetIndex];

            float inBetweenX = baseVertex.x + (rotatedVertex.x - baseVertex.x) * (samplingIndex + 1) / numOfSampling;
            float inBetweenY = baseVertex.y + (rotatedVertex.y - baseVertex.y) * (samplingIndex + 1) / numOfSampling;
            float inBetweenZ = baseVertex.z + (rotatedVertex.z - baseVertex.z) * (samplingIndex + 1) / numOfSampling;

            float depth = inBetweenZ * 1000;
            float x = inBetweenX / depth * 1000;
            float y = inBetweenY / depth * 1000;

            float rSquared = x * x + y * y;
            float rFourth = rSquared * rSquared;
            float rSixth = rFourth * rSquared;
            float distortionFactor = (1 + k2 * rSquared + k4 * rFourth + k6 * rSixth);

            float xDistorted = x * distortionFactor;
            float yDistorted = y * distortionFactor;

            float xPixel = xDistorted * fx + cx;
            float yPixel = cy - yDistorted * fy;

            CudaVector3 vertexPixel;
            vertexPixel.x = xPixel;
            vertexPixel.y = yPixel;
            vertexPixel.z = depth;

            // compute difference between computed pixel depth and actual pixel depth
            int xPixelInt = (int)vertexPixel.x;
            int yPixelInt = (int)vertexPixel.y;

            ushort actualDepth = depthImage[yPixelInt * depthImageWidth + xPixelInt];
            float computedDepth = vertexPixel.z;
            float difference = (computedDepth - actualDepth);
            float fitness = (difference * difference);

            //cache[subVertexIndex] = fitness;
            cache[subVertexIndex] = 0;

            // add fingertip information
            if (lineIndex == 2 || lineIndex == 6 || lineIndex == 10 || lineIndex == 14 || lineIndex == 18)
            {
                if (samplingIndex == numOfSampling - 1)
                {
                    int fingerIndex = 0;
                    if (lineIndex != 2)
                    {
                        fingerIndex = ((lineIndex - 6) >> 2) + 1;
                    }

                    int fingerTipX = fingerTips[fingerIndex * 2];
                    int fingerTipY = fingerTips[fingerIndex * 2 + 1];

                    float xDifference = (fingerTipX - xPixelInt);
                    xDifference *= xDifference;
                    float yDifference = (fingerTipY - yPixelInt);
                    yDifference *= yDifference;

                    float differenceSquared = (xDifference + yDifference);
                    cache[subVertexIndex] = differenceSquared * differenceSquared;
                }
            }

            thread.SyncThreads();

            // parallel reduction
            int i = numOfMaxParallelReduction / 2;
            if (subVertexIndex >= i)
            {
                cache[subVertexIndex - i] += cache[subVertexIndex];
            }

            thread.SyncThreads();

            i /= 2;

            while (i != 0)
            {
                if (subVertexIndex < i)
                {
                    cache[subVertexIndex] += cache[subVertexIndex + i];
                }
                thread.SyncThreads();
                i /= 2;
            }

            if (subVertexIndex == 0)
            {
                // update local if necessary
                if (cache[0] < pBestFitness[particleIndex])
                {
                    pBestFitness[particleIndex] = cache[0];
                    for (int d = 0; d < numOfDimensions; d++)
                    {
                        pBestPositions[particleIndex * numOfDimensions + d] = positions[particleIndex * numOfDimensions + d];
                    }
                }
            }
        }

        [Cudafy]
        public static void computeFitness2(GThread thread, float[] positions, float[] pBestFitness, float[] pBestPositions)
        {
            int particleIndex = thread.blockIdx.x;
            int offset = numOfDimensions * particleIndex;

            float fitness = 0;

            // compute fitness
            for (int d = 0; d < numOfDimensions; d++)
            {
                float value = positions[offset + d];
                fitness += (value * value);
                //fitness += f1(value);
                //fitness += f2(value);
                //fitness += (value * value - 10 * GMath.Cos(2 * GMath.PI * value) + 10);
            }


            // update local
            if (fitness < pBestFitness[particleIndex])
            {
                pBestFitness[particleIndex] = fitness;
                for (int d = 0; d < numOfDimensions; d++)
                {
                    pBestPositions[offset + d] = positions[offset + d];
                }
            }
        }

        [Cudafy]
        public static void findLocalBest(GThread thread, float[] pBestFitness, int[] pBestIndex)
        {
            float[] fitness = thread.AllocateShared<float>("fitness", numOfParticles);
            int[] cache = thread.AllocateShared<int>("cache", numOfParticles);

            int particleIndex = thread.threadIdx.x;
            fitness[particleIndex] = pBestFitness[particleIndex];
            cache[particleIndex] = particleIndex;


            thread.SyncThreads();
            int i = numOfParticles / 2;

            while (i != 0)
            {
                if (particleIndex < i)
                {
                    int index1 = cache[particleIndex];
                    int index2 = cache[particleIndex + i];

                    if (fitness[index1] < fitness[index2])
                    {
                        cache[particleIndex] = index1;
                    }
                    else
                    {
                        cache[particleIndex] = index2;
                    }
                }
                thread.SyncThreads();
                i /= 2;
            }

            if (particleIndex == 0)
            {
                pBestIndex[0] = cache[0];
            }
        }

        [Cudafy]
        public static void updateGlobalBest(GThread thread, float[] pBestPositions, float[] gBestPositions, float[] pBestFitness, float[] gBestFitness, int[] pBestIndex)
        {
            int dimension = thread.threadIdx.x;
            int bestLocalIndex = pBestIndex[0];

            float bestLocalFitness = pBestFitness[bestLocalIndex];
            float bestGlobalFitness = gBestFitness[0];

            if (bestLocalFitness < bestGlobalFitness)
            {
                if (dimension == 0)
                {
                    gBestFitness[0] = bestLocalFitness;
                }
                gBestPositions[dimension] = pBestPositions[bestLocalIndex * numOfDimensions + dimension];

            }

        }

        [Cudafy]
        public static void updateParticlePosition(GThread thread, float[] positions, float[] velocities, float[] pBestPositions, float[] gBestPositions, RandStateXORWOW[] state, int handStream)
        {
            int dimension = thread.threadIdx.x;
            int particleIndex = thread.blockIdx.x;
            int index = particleIndex * numOfDimensions + dimension;

            RandStateXORWOW localState = state[index];
            float r1 = thread.curand_uniform(ref localState);
            float r2 = thread.curand_uniform(ref localState);
            state[index] = localState;

            float position = positions[index];
            float velocity = velocities[index];
            float localBestPosition = pBestPositions[index];
            float globalBestPosition = gBestPositions[dimension];

            float omega = 0.5f;
            float c1 = 1.5f;
            float c2 = 1.5f;

            float newVelocity = omega * velocity + c1 * r1 * (localBestPosition - position) + c2 * r2 * (globalBestPosition - position);
            float newPosition = position + newVelocity;

            velocities[index] = newVelocity;

            float lowerLimit, upperLimit;

            if (handStream == (int)HandStream.Left)
            {
                lowerLimit = dev_leftLowerLimits[dimension];
                upperLimit = dev_leftUpperLimits[dimension];
            }
            else
            {
                lowerLimit = dev_rightLowerLimits[dimension];
                upperLimit = dev_rightUpperLimits[dimension];
            }

            if (newPosition > upperLimit)
            {
                newPosition = upperLimit;
            }
            else if (newPosition < lowerLimit)
            {
                newPosition = lowerLimit;
            }

            positions[index] = newPosition;
        }

        [Cudafy]
        public static CudaVertex[] dev_rightHandVertices = new CudaVertex[numOfVertices];

        [Cudafy]
        public static CudaVertex[] dev_leftHandVertices = new CudaVertex[numOfVertices];

        [Cudafy]
        public static CudaLine[] dev_lines = new CudaLine[numOfLines];

        [Cudafy]
        public struct CudaVector3
        {
            public float x;
            public float y;
            public float z;
            public float dot(CudaVector3 q)
            {
                return x * q.x + y * q.y + z * q.z;
            }
            public void subtract(CudaVector3 q)
            {
                x -= q.x;
                y -= q.y;
                z -= q.z;
            }
            public void add(CudaVector3 q)
            {
                x += q.x;
                y += q.y;
                z += q.z;
            }

            public void divide(float r)
            {
                x /= r;
                y /= r;
                z /= r;
            }

            public void rotate(CudaVector3 r0, CudaVector3 r1, CudaVector3 r2)
            {
                float newX = r0.x * x + r0.y * y + r0.z * z;
                float newY = r1.x * x + r1.y * y + r1.z * z;
                float newZ = r2.x * x + r2.y * y + r2.z * z;

                x = newX;
                y = newY;
                z = newZ;
            }
        }

        [Cudafy]
        public struct CudaLine
        {
            public int vertexIndex0;
            public int vertexIndex1;
        }

        [Cudafy]
        public struct CudaJoint
        {
            public int isIdentity;
            public CudaVector3 translation;
            public CudaVector3 axis;
            public int dimensionIndex;
        }

        [Cudafy]
        public struct CudaVertex
        {
            public CudaVector3 position;

            public CudaJoint joint0;
            public CudaJoint joint1;
            public CudaJoint joint2;
            public CudaJoint joint3;
        }

        [Cudafy]
        public static float f1(float x)
        {
            return x * x;
        }
    }
}
