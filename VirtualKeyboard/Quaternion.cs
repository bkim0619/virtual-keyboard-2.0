﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Media3D;

namespace VirtualKeyboard
{
    public class Matrix3
    {
        public Vector3[] v = new Vector3[3];

        public Matrix3(Vector3 a, Vector3 b, Vector3 c)
        {
            v[0] = a;
            v[1] = b;
            v[2] = c;
        }

        public Matrix3 multiply(Matrix3 m)
        {
            float m00 = v[0].dot(m.v[0]);
            float m01 = v[0].dot(m.v[1]);
            float m02 = v[0].dot(m.v[2]);

            float m10 = v[1].dot(m.v[0]);
            float m11 = v[1].dot(m.v[1]);
            float m12 = v[1].dot(m.v[2]);

            float m20 = v[1].dot(m.v[0]);
            float m21 = v[1].dot(m.v[1]);
            float m22 = v[1].dot(m.v[2]);

            Matrix3 toReturn = new Matrix3(new Vector3(m00, m01, m02), new Vector3(m10, m11, m12), new Vector3(m20, m21, m22));
            return toReturn;
        }

        public Vector3 multiply(Vector3 q)
        {
            float x = v[0].dot(q);
            float y = v[1].dot(q);
            float z = v[2].dot(q);

            return new Vector3(x, y, z);
        }
    }

    public class Vector3
    {
        public float X, Y, Z;

        public Vector3()
        {

        }

        public Vector3(float x, float y, float z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public float dot(Vector3 v2)
        {
            return X * v2.X + Y * v2.Y + Z * v2.Z;
        }
    }

    public class Quaternion
    {
        public Vector3 v;
        public float w;

        public Quaternion()
        {
            this.v = new Vector3();
            this.w = 0;
        }

        public Quaternion(Vector3 v, float w)
        {
            float radians = (float)(w * 2 * Math.PI / 360);
            float factor = (float)(Math.Sin(radians / 2));

            float x = (v.X * factor);
            float y = (v.Y * factor);
            float z = (v.Z * factor);

            this.v = new Vector3(x, y, z);
            this.w = (float)(Math.Cos(radians/2));
        }

        public Quaternion multiply(Quaternion q)
        {
            Quaternion r = new Quaternion();

            r.w = (w * q.w - (v.X * q.v.X + v.Y * q.v.Y + v.Z * q.v.Z));

            // cross
            Vector3 vcq = cross(v, q.v);

            float x = (v.X * q.w + q.v.X * w + vcq.X);
            float y = (v.Y * q.w + q.v.Y * w + vcq.Y);
            float z = (v.Z * q.w + q.v.Z * w + vcq.Z);

            r.v = new Vector3(x, y, z);

            return r;
        }

        public Vector3 multiply(Vector3 V)
        {
            // cross
            Vector3 vcV = cross(v, V);
            Vector3 vcvcV = cross(v, vcV);

            float x = (V.X + vcV.X * 2 * w + vcvcV.X * 2);
            float y = (V.Y + vcV.Y * 2 * w + vcvcV.Y * 2);
            float z = (V.Z + vcV.Z * 2 * w + vcvcV.Z * 2);

            return new Vector3(x, y, z);
        }

        private Vector3 cross(Vector3 a, Vector3 b)
        {
            float crossX = (a.Y * b.Z - a.Z * b.Y);
            float crossY = (a.Z * b.X - a.X * b.Z);
            float crossZ = (a.X * b.Y - a.Y * b.X);

            return new Vector3(crossX, crossY, crossZ);
        }
        
    }
}
