from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import struct
import csv
import sys

def plotHelper():
    
    with open("frametracking.csv") as f:
        content = f.readlines()

    arr = content[0].split(",")
    
    nonzeros = []
    for strVal in arr:
        
        try:
            val = float(strVal)

            if val > 0:
                nonzeros.append(val)
        except ValueError:
            continue


    print len(nonzeros)

    print min(nonzeros)
    print max(nonzeros)
    print np.mean(nonzeros)


def foo():
    cnt = 0
    data = np.zeros(512*424)

    with open("VirtualKeyboard/bin/Debug/tmp19.raw", "rb") as f:
        short = f.read(2)
        while short != "":
            # Do stuff with short.
            value = struct.unpack("H", short)[0]
            data[cnt] = value
            cnt += 1
            short = f.read(2)



    data = np.resize(data, (424,512))

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    #X = np.arange(-5, 5, 0.25)
    X = np.arange(512)
    #Y = np.arange(-5, 5, 0.25)
    Y = np.arange(424)
    X, Y = np.meshgrid(X, Y)

    d = np.random.randint(0, 2, size=(512,424))

    print data.shape
    print d.shape

    R = data[Y,X]

    #R = X + Y
    #R = np.sqrt(X**2 + Y**2)
    #Z = np.sin(R)
    Z = R

    surf = ax.plot_surface(X, Y, Z, rstride=5, cstride=5, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    ax.set_zlim(600, 1000)

    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()

def backwardProjection(M,N, depth): # focal length x, focal length y, principal point x, principal point y, radial distortion second/fourth/sixth order
    fx = 364.5652
    fy = 364.5652
    cx = 257.6964
    cy = 209.9422
    k2 = 0.09627487
    k4 = -0.2699704
    k6 = 0.08848914

    print "Input pixel coordinates: %d %d %d" % (M, N, depth)

    x = (M - cx) / fx;
    y = (cy - N) / fy;

    # distortion correction
    x0 = x;
    y0 = y;

    for j in range(4):
        r2 = x**2 + y**2
        p = 1 / (1 + k2 * r2 + k4 * r2**2 + k6 * r2 ** 3)
        x = x0 * p
        y = y0 * p

    globalX = x * depth * 0.001
    globalY = y * depth * 0.001
    globalZ = depth * 0.001

    print "Global coordinates: (%f, %f, %f)" % (globalX, globalY, globalZ)

    return (globalX, globalY, globalZ)


def forwardProjection(globalX, globalY, globalZ):
    fx = 364.5652
    fy = 364.5652
    cx = 257.6964
    cy = 209.9422
    k2 = 0.09627487
    k4 = -0.2699704
    k6 = 0.08848914

    depth = globalZ * 1000
    x = globalX / depth * 1000
    y = globalY / depth * 1000

    r2 = x**2 + y**2
    distortionFactor = (1 + k2 * r2 + k4 * r2 ** 2 + k6 * r2 ** 3)

    xDistorted = x * distortionFactor
    yDistorted = y * distortionFactor

    xPixel = xDistorted * fx + cx
    yPixel = cy - yDistorted * fy

    print "Pixel coordinates: %f %f %f" % (xPixel, yPixel, depth)


plotHelper()
#(x, y, z) = backwardProjection(275.8375, 178.2842, 732.7569)
#(x, y, z) = backwardProjection(203, 237, 754)
#forwardProjection(0.081237, 0.019092 + -0.005, 0.005 + 0.701)
#forwardProjection(0.20332, 0.022715, 0.816)




